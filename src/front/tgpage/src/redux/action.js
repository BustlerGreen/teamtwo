export const USER_LOGIN = "USER_LOGIN";
export const userLogin = function(user){return{type: USER_LOGIN, payload: {user},}};
export const USER_LOGOUT = "USER_LOGOUT";
export const userLogout = function(){return{type: USER_LOGOUT, payload: {}}};