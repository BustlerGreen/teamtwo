@echo off
REM Стартовый шаблон инициализации решения на базе миксросервисов .NET
@chcp 65001 >> nul
@REM !!! - Внимание - установите свои параметры - !!!
@REM Задаем имя решения
set SolutionName=EComm
@if exist %SolutionName% goto exitError

REM Инициализируем решение
dotnet new sln -o %SolutionName% -n %SolutionName%
dotnet new gitignore -o %SolutionName%
dotnet new editorconfig -o %SolutionName%

cd %SolutionName%
mkdir API
mkdir Core
mkdir Services
mkdir Integrations

REM Инициализируем библиотеку инфраструктурного кода
dotnet new classlib -n Domain -o Core\Domain
dotnet new classlib -n Application -o Core\Application
dotnet new classlib -n Infrastructure -o Core\Infrastructure

REM Инициализируем проект - Шлюз приложения
dotnet new webapi --no-https -n Gateway.Api -o API\Gateway.Api
dotnet new nunit -n Gateway.Api.Test -o API\Gateway.Api.Test

REM Инициализируем проект - Сервис управления учетными записями
dotnet new webapi --no-https -n Identity.Api -o Services\Identity.Api
dotnet new nunit -n Identity.Api.Test -o Services\Identity.Api.Test

REM Инициализируем проект - Сервис управления учетными записями - модуль доступа к данным
dotnet new classlib -n Identity.DataProvider -o Services\Identity.DataProvider

REM Инициализируем проект - Сервис каталога товаров
dotnet new webapi --no-https -n Products.Api -o Services\Products.Api
dotnet new classlib -n Products.DataProvider -o Services\Products.DataProvider

REM Инициализируем проект - Сервис заказов
dotnet new webapi --no-https -n Orders.Api -o Services\Orders.Api
dotnet new classlib -n Orders.DataProvider -o Services\Orders.DataProvider

REM - Добавляем проекты в решение
dotnet sln add .\Core\Domain\ .\Core\Application\ .\Core\Infrastructure\ .\API\Gateway.Api\ .\API\Gateway.Api.Test\  .\Services\Identity.Api\ .\Services\Identity.Api.Test\ .\Services\Identity.DataProvider\ .\Services\Products.Api\  .\Services\Products.DataProvider\ .\Services\Orders.Api\ .\Services\Orders.DataProvider\

REM Добавляем в проекты ссылки на необходимые проекты
dotnet add .\Core\Application\ reference .\Core\Domain\
dotnet add .\Core\Infrastructure\ reference .\Core\Application\
dotnet add .\API\Gateway.Api\ reference .\Core\Infrastructure\
dotnet add .\API\Gateway.Api.Test\ reference .\API\Gateway.Api\
dotnet add .\Services\Identity.DataProvider\ reference .\Core\Infrastructure\
dotnet add .\Services\Identity.Api\ reference .\Services\Identity.DataProvider\
dotnet add .\Services\Identity.Api.Test\ reference .\Services\Identity.Api\

REM - Установка пакетов для проекта домена
dotnet add .\Core\Domain\ package Microsoft.AspNetCore.Identity
dotnet add .\Core\Domain\ package Microsoft.AspNetCore.Identity.EntityFrameworkCore

REM - Установка пакетов для проекта приложения
dotnet add .\Core\Application\ package System.IdentityModel.Tokens.Jwt
dotnet add .\Core\Application\ package Microsoft.Extensions.Configuration.Binder
dotnet add .\Core\Application\ package Microsoft.IdentityModel.Tokens
dotnet add .\Core\Application\ package Microsoft.AspNetCore.Authentication.JwtBearer

REM - Установка пакетов для проекта инфраструктуры
dotnet add .\Core\Infrastructure\ package MassTransit
dotnet add .\Core\Infrastructure\ package MassTransit.AspNetCore
dotnet add .\Core\Infrastructure\ package MassTransit.Extensions.DependencyInjection
dotnet add .\Core\Infrastructure\ package MassTransit.RabbitMQ
dotnet add .\Core\Infrastructure\ package Microsoft.AspNetCore.Authentication.JwtBearer
dotnet add .\Core\Infrastructure\ package Microsoft.Extensions.Configuration.Abstractions
dotnet add .\Core\Infrastructure\ package Microsoft.Extensions.Configuration.Binder
dotnet add .\Core\Infrastructure\ package Microsoft.IdentityModel.Tokens
dotnet add .\Core\Infrastructure\ package System.IdentityModel.Tokens.Jwt
dotnet add .\Core\Infrastructure\ package Polly
dotnet add .\Core\Infrastructure\ package StackExchange.Redis
dotnet add .\Core\Infrastructure\ package Npgsql.EntityFrameworkCore.PostgreSQL
dotnet add .\Core\Infrastructure\ package Microsoft.EntityFrameworkCore.Design
dotnet add .\Core\Infrastructure\ package Serilog.AspNetCore
dotnet add .\Core\Infrastructure\ package Serilog.Enrichers.Environment
dotnet add .\Core\Infrastructure\ package Serilog.Sinks.Elasticsearch

REM - Установка пакетов для проекта Identity.Api
dotnet add .\Services\Identity.Api\ package Microsoft.EntityFrameworkCore.Design
dotnet add .\Services\Identity.DataProvider\ package Microsoft.AspNetCore.Identity

REM - Установка дополнительных пакетов для проекта шлюза приложения
dotnet add .\API\Gateway.Api\ package Ocelot
dotnet add .\API\Gateway.Api\ package Ocelot.Cache.CacheManager

REM - Установка дополнительных пакетов для тестировочных проектов
dotnet add .\API\Gateway.Api.Test\ package Moq
dotnet add .\Services\Identity.Api.Test\ package Moq

REM Сервис каталога товаров
dotnet add .\Services\Products.DataProvider\ package AutoMapper
dotnet add .\Services\Products.DataProvider\ package AutoMapper.Extensions.Microsoft.DependencyInjection
dotnet add .\Services\Products.Api\ package Microsoft.EntityFrameworkCore.Design
dotnet add .\Services\Products.DataProvider\ reference .\Core\Infrastructure\
dotnet add .\Services\Products.Api\ reference .\Services\Products.DataProvider\

REM Сервис заказов
dotnet add .\Services\Orders.DataProvider\ package AutoMapper
dotnet add .\Services\Orders.DataProvider\ package AutoMapper.Extensions.Microsoft.DependencyInjection
dotnet add .\Services\Orders.Api\ package Microsoft.EntityFrameworkCore.Design
dotnet add .\Services\Orders.DataProvider\ reference .\Core\Infrastructure\
dotnet add .\Services\Orders.Api\ reference .\Services\Orders.DataProvider\


REM - обновление зависимостей решения
dotnet restore

@echo off
goto exitNormal
:exitError
@echo on
@echo Директория с именем решения уже существует!
@echo off
:exitNormal
@echo Инициализация решения закончена.
exit