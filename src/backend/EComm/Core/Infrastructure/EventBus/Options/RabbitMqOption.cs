namespace Infrastructure.EventBus.Options
{
    public class RabbitMqOption
    {
        public string ConnectionString { get; set; } = string.Empty;
        public string Username { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
    }
}