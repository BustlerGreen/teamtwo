using Domain.Entities;

namespace Infrastructure.Authentication
{
    public interface IAuthenticationHandler
    {
        string CreateAuthToken(User user, IList<string> roles);
        RefreshToken GenerateRefreshToken();
    }
}