namespace Infrastructure.Authentication
{
    public class JwtAuthTokenOptions
    {
        public string? SecretKey { get; set; }
        public int ExpiryMinutes { get; set; }
        public string? Issuer { get; set; }
    }
}