using System.Linq.Expressions;

namespace Infrastructure.Persistence
{
    public interface IGenericRepository<T> where T : class

    {
        public Task<List<T>> FindAll();
        public Task<List<T>> FindByCondition(Expression<Func<T, bool>> expression);
        public Task<T> Create(T entity);
        public Task AddRangeAsync(List<T> entities);
        public Task Update(T entity);
        public Task Delete(T entity);
    }
}