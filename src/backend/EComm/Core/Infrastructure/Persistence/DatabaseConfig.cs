namespace Infrastructure.Persistence
{
    public class DatabaseConfig
    {
        public string ConnectionString { get; set; } = string.Empty;
        public string Database { get; set; } = string.Empty;
    }
}