namespace Infrastructure.Persistence
{
    public interface IDatabaseInitializer
    {
        public Task InitializeAsync();
    }
}