﻿

namespace Infrastructure.Contracts
{
    public class GetCartMessage
    {
        public Guid Id { get; set; }
    }

    public class AddCartMessage
    {
        public Guid Id { get; set; }
        public string? UserName { get; set; }
        public List<CartItemMessage>? Items { get; set; }
    }

    public class RemoveCartMessage
    {
        public Guid Id { get; set; }
    }

    public class AddItemToCartMessage
    {
        public Guid CartId { get; set; }
        public Guid ItemId { get; set; }
        public string? Name { get; set; }
        public float Price { get; set; }
    }
    
    public class CartItemMessage
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public float Price { get; set; }
        public int Qunatity { get; set; }
    }
    
    public class CartStatusMessage
    {
        public Guid Id { get; set; }
        public List<CartItemMessage>? CartItems { get; set; }
    }
}
