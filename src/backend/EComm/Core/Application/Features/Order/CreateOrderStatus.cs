namespace Application.Features.Order
{
    public class CreateOrderStatus
    {
        public string Key { get; set; } = string.Empty;
        public string Value { get; set; } = string.Empty;
    }
}