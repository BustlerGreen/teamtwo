#nullable disable

namespace Application.Features.Order
{
    public class CreateOrderItem
    {
        public string Name { get; set; } = string.Empty;
        public int Quantity { get; set; }
        public Guid ProductId { get; set; }
    }
}