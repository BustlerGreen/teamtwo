using Domain.Entities;

#nullable disable

namespace Application.Features.Order
{
    public class CreateOrder
    {
        public string Name { get; set; } = string.Empty;
        public string Address { get; set; } = string.Empty;
        public ICollection<OrderItem> Items { get; set; } = null;
        public Guid UserId { get; set; }
    }
}