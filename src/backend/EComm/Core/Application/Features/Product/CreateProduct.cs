#nullable disable

namespace Application.Features.Product
{
    public class CreateProduct
    {
        public string Name { get; set; }
        public string Description { get; set; }
        Single Price { get; set; }
    }
}