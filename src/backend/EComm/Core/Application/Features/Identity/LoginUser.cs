namespace Application.Features.Identity
{
    public class LoginUser
    {
        public string Username { get; set; } = string.Empty;

        public string Password { get; set; } = string.Empty;
    }
}