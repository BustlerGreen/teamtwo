using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Features.Identity
{
    /// <summary>
    ///  Модель для создания/регистрации пользователя
    ///  В обязательном порядке проверять поля на бекенде
    /// </summary>
    public class CreateUser
    {
        [Required]
        public string Username { get; set; } = string.Empty;

        [Required]
        public string Email { get; set; } = string.Empty;

        public string Phone { get; set; } = string.Empty;

        [Required]
        [RegularExpression("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[^a-zA-Z\\d]).{8,}$", ErrorMessage = "Password must be complex")]
        public string Password { get; set; } = string.Empty;
    }
}