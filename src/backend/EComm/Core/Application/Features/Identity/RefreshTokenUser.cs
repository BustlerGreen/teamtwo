namespace Application.Features.Identity
{
    public class RefreshTokenUser
    {
        public string Username { get; set; } = string.Empty;
        public string Token { get; set; } = string.Empty;
    }
}