using System.ComponentModel.DataAnnotations;

namespace Application.Features.Identity
{
    public class GetUserByUsername
    {
        [Required]
        public string Username { get; set; } = string.Empty;
    }
}