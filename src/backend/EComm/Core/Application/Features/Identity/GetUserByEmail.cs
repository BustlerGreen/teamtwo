using System.ComponentModel.DataAnnotations;

namespace Application.Features.Identity
{
    public class GetUserByEmail
    {
        [Required]
        public string Email { get; set; } = string.Empty;
    }
}