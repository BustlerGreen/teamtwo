#nullable disable

namespace Domain.Events
{
    public class OrderStatusCreatedEvent
    {
        public int Id { get; set; }
        public string Key { get; set; } = string.Empty;
        public string Value { get; set; } = string.Empty;
    }
}