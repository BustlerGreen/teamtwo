#nullable disable

namespace Domain.Events
{
    public class ProductCreatedEvent
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Single Price { get; set; }
    }
}