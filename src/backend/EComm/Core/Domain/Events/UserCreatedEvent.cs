namespace Domain.Events
{
    public class UserCreatedEvent
    {
        public Guid Id { get; set; } = Guid.Empty;
        public string Username { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public IList<string> Roles { get; set; } = new List<string>();
        public string Token { get; set; } = string.Empty;
        public string RefreshToken { get; set; } = string.Empty;

        
    }
}