#nullable disable

using Domain.Entities;

namespace Domain.Events
{
    public class OrderCreatedEvent
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        string Name { get; set; }
        DateTime CreationDate { get; set; }
        DateTime? ExecutionDate { get; set; }
        public string Address { get; set; }
        public int OrderStatus { get; set; }
        public ICollection<OrderItem> Items { get; set; }

    }
}