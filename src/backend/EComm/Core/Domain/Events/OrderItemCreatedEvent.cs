#nullable disable

namespace Domain.Events
{
    public class OrderItemCreatedEvent
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public string Name { get; set; } = string.Empty;
        public int Quantity { get; set; }
    }
}