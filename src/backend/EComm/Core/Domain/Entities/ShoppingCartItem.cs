﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Domain.Common;

namespace Domain.Entities
{
    public class ShoppingCartItem: BaseEntity<Guid>
    {
        public string? Name { get; set; }
        public Single Price { get; set; }
        public string? Description { get; set; }
        public int Quantity { get; set; }

    }
}
