using Microsoft.AspNetCore.Identity;

#nullable disable

namespace Domain.Entities
{
    public class User : IdentityUser<Guid>
    {
        public User() { }
        public User(string username, string email, string phone)
        {
            UserName = username;
            Email = email;
            PhoneNumber = phone;
        }
        public ICollection<RefreshToken> RefreshTokens { get; set; } = new List<RefreshToken>();
    }
}