using Domain.Common;

#nullable disable

namespace Domain.Entities
{
    public class Order : BaseEntity<Guid>
    {
        public string Name { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? ExecutionDate { get; set; }

        public string Address { get; set; }

        public int OrderStatus { get; set; } // OrderStatus Key

        public ICollection<OrderItem> Items { get; set; }

        public Guid UserId { get; set; }
        public virtual User User { get; set; }
    }

    public class OrderItem : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public int Quantity { get; set; }

        public Guid ProductId { get; set; }
        public virtual Product Product { get; set; }
    }

    public class OrderStatus : BaseReferenceEntity<int, string>
    {

    }
}