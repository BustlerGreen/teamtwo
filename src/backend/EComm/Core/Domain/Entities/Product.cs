using Domain.Common;

#nullable disable

namespace Domain.Entities
{
    public class Product : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Single Price { get; set; }
    }
}