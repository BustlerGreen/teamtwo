namespace Domain.Common.Interfaces
{
    public interface IReferenceEntity<TKey> where TKey : IEquatable<TKey>
    {
        string Key { get; set; }
        TKey Value { get; set; }
    }
}