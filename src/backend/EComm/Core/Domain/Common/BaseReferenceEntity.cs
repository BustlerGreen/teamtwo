using Domain.Common.Interfaces;
#nullable disable
namespace Domain.Common
{
    public class BaseReferenceEntity<TKey, TReference> : BaseEntity<TKey>, IReferenceEntity<TReference> where TKey : IEquatable<TKey> where TReference : IEquatable<TReference>
    {
        public string Key { get; set; }
        public TReference Value { get; set; }
    }
}