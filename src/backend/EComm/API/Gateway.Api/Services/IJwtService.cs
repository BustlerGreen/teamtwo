﻿using System.Security.Principal;
using Application.Features.Identity;
using Microsoft.AspNetCore.Identity;

namespace Gateway.Api.Services
{
    public interface IJwtService
    {
        Task<IdentityResult> RegisterUserAsync(CreateUser user);
        Task<bool> ValidateUserAsync(LoginUser user);
        Task<string> CreateTokenAsync();
    }
}
