﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

using Application.Features.Identity;

using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace Gateway.Api.Services
{
    public class JwtService :IJwtService
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _configuration;
        private IdentityUser? _user;

        public JwtService(UserManager<IdentityUser> user, IConfiguration configuration)
        { 
            _userManager = user;
            _configuration = configuration;
        }

        public async Task<IdentityResult> RegisterUserAsync(CreateUser createUser)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = createUser.Username,
                Email = createUser.Email,
                PhoneNumber = createUser.Phone,
            };
            var res = await _userManager.CreateAsync(user, createUser.Password);
            return res;
        }

        public async Task<bool> ValidateUserAsync(LoginUser loginUser)
        {
            _user = await _userManager.FindByNameAsync(loginUser.Username);
            if (null == _user) return false;
            var res = await _userManager.CheckPasswordAsync(_user, loginUser.Password);
            return res;
        }

        public async Task<string> CreateTokenAsync()
        {
            var signingCredentials = GetSigningCredentials();
            var claims = await GetClaims();
            var token = CreateSecurityToken(signingCredentials, claims);
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private SigningCredentials GetSigningCredentials()
        {
            var jwtConfig = _configuration.GetSection("AuthOptions");
            var secret = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtConfig["Key"]));
            return new SigningCredentials(secret, SecurityAlgorithms.HmacSha256);
        }

        private async Task<List<Claim>> GetClaims()
        {
            var claims = new List<Claim>() {new Claim(ClaimTypes.Name, _user.UserName) };
            var roles = await _userManager.GetRolesAsync(_user);
            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }
            return claims;
        }

        private JwtSecurityToken CreateSecurityToken(SigningCredentials signingCredentials, List<Claim> claims)
        {
            var jwtConfig = _configuration.GetSection("AuthOptions");
            var secToken = new JwtSecurityToken(
                issuer: jwtConfig["Issuer"],
                audience: jwtConfig["Audience"],
                claims: claims,
                expires: DateTime.Now.AddMinutes(Convert.ToDouble(jwtConfig["LifeTimeMinute"])),
                signingCredentials: signingCredentials
                );
            return secToken;
        }
    }
}
