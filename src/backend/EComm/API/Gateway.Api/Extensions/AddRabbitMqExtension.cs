﻿using Application.Features.Basket;
using Application.Features.Order;
using Application.Features.Product;

using Infrastructure.EventBus.Options;

using MassTransit;

namespace Gateway.Api.Extensions
{
    public static class AddRabbitMqExtension
    {
        public static void AddRabbitMq(this IServiceCollection services, IConfiguration configuration)
        {
            var rabbitMq = new RabbitMqOption();
            configuration.GetSection("rabbitmq").Bind(rabbitMq);

            services.AddMassTransit(mtCfg =>
            {
                mtCfg.AddBus(bus => Bus.Factory.CreateUsingRabbitMq(mqCfg =>
                {
                    mqCfg.Host(new Uri(rabbitMq.ConnectionString), hostCfg =>
                    {
                        hostCfg.Username(rabbitMq.Username);
                        hostCfg.Password(rabbitMq.Password);
                    });
                    mqCfg.ConfigureEndpoints(bus);
                }));
                mtCfg.AddRequestClient<GetCartMessage>(new Uri(@"rabbitmq://rabbitmq/getbasket"));
               
                mtCfg.AddRequestClient<GetProducts>(new Uri(@"rabbitmq://rabbitmq/get_products"));
                mtCfg.AddRequestClient<GetProduct>(new Uri(@"rabbitmq://rabbitmq/get_product"));

                mtCfg.AddRequestClient<GetOrders>(new Uri(@"rabbitmq://rabbitmq/get_orders"));
                mtCfg.AddRequestClient<GetOrder>(new Uri(@"rabbitmq://rabbitmq/get_order"));

                mtCfg.AddRequestClient<GetOrderItems>(new Uri(@"rabbitmq://rabbitmq/get_order_items"));
                mtCfg.AddRequestClient<GetOrderItem>(new Uri(@"rabbitmq://rabbitmq/get_order_item"));

                mtCfg.AddRequestClient<GetOrderStatuses>(new Uri(@"rabbitmq://rabbitmq/get_order_statuses"));
                mtCfg.AddRequestClient<GetOrderStatus>(new Uri(@"rabbitmq://rabbitmq/get_order_status"));
            });
        }
    }
}
