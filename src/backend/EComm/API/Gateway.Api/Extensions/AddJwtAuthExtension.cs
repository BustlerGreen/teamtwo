﻿using Gateway.Api.Services;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Gateway.Api.Extensions
{
    public static class AddJwtAuthExtension
    {
        public static IServiceCollection AddJwtAuth(this IServiceCollection services, IConfiguration configurtion)
        {
            var AuthenicationProviderKey = "EcommKey";
            var JwtConfig = configurtion.GetSection("AuthOptions");
            services.AddScoped<IJwtService, JwtService>();
            services.AddAuthentication("Bearer")
            //    cfgopt =>
            //{
            //    cfgopt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    cfgopt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //})
              .AddJwtBearer(AuthenicationProviderKey, config =>
              {
                  config.SaveToken = false;
                  config.TokenValidationParameters = new TokenValidationParameters()
                  {
                      ValidateIssuer = true,
                      ValidateAudience = false,
                      ValidateLifetime = true,
                      ValidateIssuerSigningKey = true,
                      ValidIssuer = JwtConfig["Issuer"],
                      ValidAudience = JwtConfig["Audience"],
                      IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JwtConfig["Key"]))
                  };
              });

            
            return services;

        }
    }
}
