using System.Reflection;
using System.Text;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Gateway.Api.Extensions;

using Ocelot.DependencyInjection;
using Gateway.Api.Services;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

var assembly = typeof(Program).Assembly.GetName().Name;
var connStr = builder.Configuration.GetConnectionString("PgUser");
builder.Services.AddDbContext<IdentityDbContext>(options =>
{
    options.UseNpgsql(connStr, optbld => optbld.MigrationsAssembly(assembly));
});
builder.Services.AddIdentity<IdentityUser, IdentityRole>()
    .AddEntityFrameworkStores<IdentityDbContext>();

builder.Configuration.AddJsonFile("configuration.json", optional: false, reloadOnChange: true);
builder.Services.AddOcelot(builder.Configuration);

builder.Services.AddJwtAuth(builder.Configuration);

builder.Services.AddRabbitMq(builder.Configuration);



builder.Services.AddCors(option =>
    option.AddPolicy(
        name: "allowUI",
        policy =>
        {
            policy.AllowAnyOrigin()//WithOrigins("http://localhost:3000", "https://localhost:3000", "http://bustlergreen.ru", "https://bustlergreen.ru")
            .WithMethods("post", "get", "put")
            .AllowAnyHeader();
            //.AllowCredentials();
        }
    )
);

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddControllers();

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var context = scope.ServiceProvider.GetRequiredService<IdentityDbContext>();
    try
    {
        context.Database.Migrate();
    }
    catch { };
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseAuthentication();
app.UseAuthorization();
app.UseCors("allowUI");

app.MapControllers();

app.Run();
