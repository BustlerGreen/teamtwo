﻿using Application.Common.Models;
using Application.Features.Product;
using Domain.Events;
using MassTransit;
using Microsoft.AspNetCore.Mvc;

namespace Gateway.Api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IBusControl _bus;
        private readonly IRequestClient<GetProduct> _getProduct;
        private readonly IRequestClient<GetProducts> _getProducts;
        public ProductsController(IRequestClient<GetProducts> getProducts, IRequestClient<GetProduct> getProduct, IBusControl bus)
        {
            _getProduct = getProduct;
            _getProducts = getProducts;
            _bus = bus;
        }

        [HttpGet]
        public async Task<ActionResult<List<ProductCreatedEvent>>> GetProducts()
        {
            try
            {
                //todo: realize pagination on GetProducts input parameter
                var prodrange = new GetProducts();
                var prods = await _getProducts.GetResponse<Result<List<ProductCreatedEvent>>>(prodrange);
                if (prods.Message is null) return NotFound();
                return Ok(prods.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{Id:guid}")]
        public async Task<ActionResult<ProductCreatedEvent>> GetProduct(Guid Id)
        {
            try
            {
                var prod = await _getProduct.GetResponse<Result<ProductCreatedEvent>>(new GetProduct { Id = Id});
                if (null != prod.Message) return NotFound();
                return Ok(prod.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateProduct(CreateProduct product)
        {
            var endPoint = await _bus.GetSendEndpoint(new Uri(@"rabbitmq://localhost/create_product"));
            if (null == endPoint) return StatusCode(500);
            await endPoint.Send(product);
            return Accepted();
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteProduct(Guid Id)
        {
            var endPoint = await _bus.GetSendEndpoint(new Uri(@"rabbitmq://localhost/delete_product"));
            if (null == endPoint) return StatusCode(500);
            await endPoint.Send(new DeleteProduct { Id = Id });
            return Ok();
        }
    }
}
