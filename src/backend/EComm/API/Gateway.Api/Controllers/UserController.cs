﻿using Application.Features.Identity;
using Gateway.Api.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Gateway.Api.Controllers
{
    [ApiController]
    [Route("/api/v1/[controller]")]
    public class UserController: ControllerBase
    {
        private readonly IJwtService _jwtService;
        public UserController(IJwtService jwtService) 
        {
            _jwtService = jwtService;
        }

        [HttpPost("Register")]
        public async Task<ActionResult> Register(CreateUser createUser)
        {
            var res = await _jwtService.RegisterUserAsync(createUser);
            return !res.Succeeded ? new BadRequestObjectResult(res) : Ok(res);
        }

        [HttpPost("Login")]
        public async Task<ActionResult> Login([FromForm] LoginUser loginUser)
        {
            var res = await _jwtService.ValidateUserAsync(loginUser);
            return !res ? NotFound() : Ok(new { token = await _jwtService.CreateTokenAsync() });
        }
    }
}
