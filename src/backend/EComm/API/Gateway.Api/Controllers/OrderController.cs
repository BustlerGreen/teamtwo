﻿using Application.Common.Models;
using Application.Features.Order;

using Domain.Events;

using MassTransit;

using Microsoft.AspNetCore.Mvc;

namespace Gateway.Api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class OrderController :ControllerBase
    {
        private readonly IBusControl _bus;
        private readonly IRequestClient<GetOrder> _getOrder;
        private readonly IRequestClient<GetOrders> _getOrders;
        public OrderController(IBusControl bus, IRequestClient<GetOrders> getOrders, IRequestClient<GetOrder> getOrder) 
        { 
            _bus = bus;
            _getOrder = getOrder;
            _getOrders = getOrders;
        }

        [HttpGet]
        public async Task<ActionResult<List<OrderCreatedEvent>>> GetOrders()
        {
            try
            {

                var orders = await _getOrders.GetResponse<Result<List<OrderCreatedEvent>>>(new GetOrders()) ;
                if (null != orders.Message) return NotFound();
                return Ok(orders.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{Id:guid}")]
        public async Task<ActionResult<OrderCreatedEvent>> GetOrder(Guid Id)
        {
            try
            {
                var order = await _getOrder.GetResponse<Result<OrderCreatedEvent>>(new GetOrder { Id = Id });
                if (null == order.Message) return NotFound();
                return Ok(order.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult> CreateOrder(CreateOrder order)
        {
            if (null == order) return BadRequest();
            var endPoint = await _bus.GetSendEndpoint(new Uri(@"rabbitmq://localhost/create_order"));
            if (null == endPoint) return StatusCode(500);
            await endPoint.Send(order);
            return Accepted();
        }
        
        [HttpDelete("{Id:guid}")]
        public async Task<ActionResult> DeleteOrder(Guid Id)
        {
            var endPoint = await _bus.GetSendEndpoint(new Uri(@"rabbitmq://localhost/delete_order"));
            if(null == endPoint) return StatusCode(500);
            await endPoint.Send(new  GetOrder { Id = Id });
            return Accepted();
        }
    }
}
