﻿using Domain.Entities;

using Infrastructure.Contracts;

using MassTransit;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Gateway.Api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    
    public class BasketController : ControllerBase
    {
        private readonly IRequestClient<GetCartMessage> _getCartClient;
        private readonly IBusControl _bus;


        public BasketController(IRequestClient<GetCartMessage> getCartClient, IBusControl bus)
        { 
            _getCartClient = getCartClient;
           _bus = bus;
        }

        [HttpGet("{Id:guid}")]
        public async Task<ActionResult<ShoppingCart>> GetCartById(Guid Id)
        {
            try
            {
                var cart =  await _getCartClient.GetResponse<CartStatusMessage>(new GetCartMessage{ Id = Id });
                if (cart.Message.Id == Guid.Empty) return NotFound();
                return Ok(cart.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult<ShoppingCart>> CreateCart(AddCartMessage cart)
        {
            if(null == cart) return BadRequest();
            var endPoint = await _bus.GetSendEndpoint(new Uri(@"rabbitmq://localhost/addbasket"));

            await endPoint.Send(cart);
            return Ok();
        }


        [HttpDelete("{Id:guid}")]
        public async Task<ActionResult> DeleteCart(Guid Id)
        {
            var endPoint = await _bus.GetSendEndpoint(new Uri(@"rabbitmq://localhost/removebasket"));
            await endPoint.Send(new RemoveCartMessage{ Id = Id});
            return Ok();
        }
    }
}
