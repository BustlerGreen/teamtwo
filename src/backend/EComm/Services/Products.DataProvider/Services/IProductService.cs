using Application.Common.Models;
using Application.Features.Product;
using Domain.Events;

namespace Products.DataProvider.Services
{
    public interface IProductService
    {
        Task<Result<ProductCreatedEvent>> Create(CreateProduct dto);
        Task<Result<ProductCreatedEvent>> Delete(DeleteProduct dto);
        Task<Result<ProductCreatedEvent>> Get(GetProduct dto);
        Task<Result<List<ProductCreatedEvent>>> GetAll(GetProducts dto);
    }
}