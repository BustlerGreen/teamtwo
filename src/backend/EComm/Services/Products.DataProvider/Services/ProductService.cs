using Application.Common.Models;
using Application.Features.Product;
using AutoMapper;
using Domain.Entities;
using Domain.Events;
using Products.DataProvider.Repositories;

namespace Products.DataProvider.Services
{
    public class ProductService : IProductService
    {
        
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IProductRepository _serviceRepository;
        public ProductService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _serviceRepository = _unitOfWork.ProductRepository;
        }
        public async Task<Result<ProductCreatedEvent>> Create(CreateProduct dto)
        {
            var existedEntity = await _serviceRepository.FindByCondition(e => e.Name == dto.Name);

            if (existedEntity.Count == 0 || existedEntity == null)
            {
                var newEntity =_mapper.Map<Product>(dto);

                try
                {
                    await _serviceRepository.Create(newEntity);
                    await _unitOfWork.SaveAsync();

                    var newCreatedEvent = _mapper.Map<ProductCreatedEvent>(newEntity);
                    return Result<ProductCreatedEvent>.Success(newCreatedEvent);
                }
                catch (Exception ex)
                {
                    return Result<ProductCreatedEvent>.Failure($"Object {typeof(Product)} creation error - {ex.Message}");
                }

            }
            return Result<ProductCreatedEvent>.Failure($"Object {typeof(Product)} creation error - such object already exists");
        }

        public async Task<Result<ProductCreatedEvent>> Delete(DeleteProduct dto)
        {
            var existedEntity = await _serviceRepository.FindByCondition(e => e.Id == dto.Id);

            if (existedEntity.Count == 0 || existedEntity == null)
            {
                return Result<ProductCreatedEvent>.Failure($"Error deleting object {typeof(Product)} - no such object exists");
            }

            try
            {
                var deletedEntity = existedEntity.FirstOrDefault();
                await _serviceRepository.Delete(deletedEntity!);
                await _unitOfWork.SaveAsync();

                var newCreatedEvent =_mapper.Map<ProductCreatedEvent>(deletedEntity);
                return Result<ProductCreatedEvent>.Success(newCreatedEvent);
            }
            catch (Exception ex)
            {
                return Result<ProductCreatedEvent>.Failure($"Error deleting object {typeof(Product)} - {ex.Message}");
            }
        }

        public async Task<Result<ProductCreatedEvent>> Get(GetProduct dto)
        {
            var existedEntity = await _serviceRepository.FindByCondition(e => e.Id == dto.Id);

            if (existedEntity.Count == 0 || existedEntity == null)
            {
                return Result<ProductCreatedEvent>.Failure($"Object {typeof(Product)} request failed - no such object exists");
            }

            var requestedEntity = existedEntity.FirstOrDefault();

            var newCreatedEvent = _mapper.Map<ProductCreatedEvent>(requestedEntity);
            return Result<ProductCreatedEvent>.Success(newCreatedEvent);
        }

        public async Task<Result<List<ProductCreatedEvent>>> GetAll(GetProducts dto)
        {
            var existedEntities = await _serviceRepository.FindAll();

            if (existedEntities?.Count == 0 || existedEntities == null)
            {
                return Result<List<ProductCreatedEvent>>.Failure($"Object {typeof(Product)} request failed - no such objects exists");
            }

            //var newCreatedEvent = _mapper.Map<List<ProductCreatedEvent>>(existedEntities);
            var newCreatedEvent = new List<ProductCreatedEvent>();
            newCreatedEvent.AddRange(existedEntities.Select(item => new ProductCreatedEvent
            {
                Id = item.Id,
                Price = item.Price,
                Name = item.Name,
                Description = item.Description
            }).ToList());

            return Result<List<ProductCreatedEvent>>.Success(newCreatedEvent);
        }
    }
}