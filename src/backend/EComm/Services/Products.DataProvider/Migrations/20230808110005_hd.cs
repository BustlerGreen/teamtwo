﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Products.DataProvider.Migrations
{
    /// <inheritdoc />
    public partial class hd : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Description", "Name", "Price" },
                values: new object[,]
                {
                    { new Guid("17365e4d-90ba-4572-a034-e11b0099cd58"), "спортинвентарь", "гиря 16 пудов", 3.36f },
                    { new Guid("29b0ce25-bb97-4804-95b6-4118663ff9a8"), "спортинвентарь", "гантели", 23.36f },
                    { new Guid("4c88d445-c6cd-4de1-b38d-c54ced261cbc"), "спортинвентарь", "штанга", 523.36f },
                    { new Guid("6d615265-d1f1-4e64-91f7-6c4f29e8c1bd"), "добавки и бады", "жирожог", 923.36f },
                    { new Guid("a5444c96-fd1a-4de4-bacd-1e90b1215290"), "добавки и бады", "пузоерайзер", 12923.36f }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("17365e4d-90ba-4572-a034-e11b0099cd58"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("29b0ce25-bb97-4804-95b6-4118663ff9a8"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("4c88d445-c6cd-4de1-b38d-c54ced261cbc"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("6d615265-d1f1-4e64-91f7-6c4f29e8c1bd"));

            migrationBuilder.DeleteData(
                table: "Products",
                keyColumn: "Id",
                keyValue: new Guid("a5444c96-fd1a-4de4-bacd-1e90b1215290"));
        }
    }
}
