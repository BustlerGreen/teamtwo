using System.Xml.Linq;

using Domain.Entities;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Products.DataProvider.Persistence
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            this.ChangeTracker.LazyLoadingEnabled = false;
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            this.ChangeTracker.LazyLoadingEnabled = false;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Product>().
                HasData(
                new Product { Id = Guid.NewGuid(), Description = "��������������", Name = "�������", Price = 23.36f },
                new Product { Id = Guid.NewGuid(), Description = "��������������", Name = "������", Price = 523.36f },
                new Product { Id = Guid.NewGuid(), Description = "��������������", Name = "���� 16 �����", Price = 3.36f },
                new Product { Id = Guid.NewGuid(), Description = "������� � ����", Name = "�������", Price = 923.36f },
                new Product { Id = Guid.NewGuid(), Description = "������� � ����", Name = "�����������", Price = 12923.36f }
                );
        }

        public virtual DbSet<Product> Products { get; set; }

    }
}