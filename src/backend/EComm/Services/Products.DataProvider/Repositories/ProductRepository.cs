using Domain.Entities;
using Products.DataProvider.Persistence;

namespace Products.DataProvider.Repositories
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(DatabaseContext context) : base(context)
        {
        }
    }
}