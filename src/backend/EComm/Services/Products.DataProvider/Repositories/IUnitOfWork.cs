namespace Products.DataProvider.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        IProductRepository ProductRepository { get; }
        Task SaveAsync();
    }
}