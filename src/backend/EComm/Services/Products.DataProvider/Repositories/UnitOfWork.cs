using Products.DataProvider.Persistence;

#nullable disable

namespace Products.DataProvider.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DatabaseContext _context;
        public UnitOfWork(DatabaseContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        #region Properties
        private ProductRepository _productRepository;
        public IProductRepository ProductRepository => _productRepository ?? (_productRepository = new ProductRepository(_context));
        #endregion

        #region Methods
        public async Task SaveAsync() => await _context.SaveChangesAsync();
        #endregion

        #region Implements IDisposable
        public void Dispose()
        {
            _context.Dispose();
        }
        #endregion
    }
}