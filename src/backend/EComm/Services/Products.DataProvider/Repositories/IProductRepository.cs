using Domain.Entities;
using Infrastructure.Persistence;

namespace Products.DataProvider.Repositories
{
    public interface IProductRepository : IGenericRepository<Product>
    {

    }
}