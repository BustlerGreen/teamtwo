using System.Text;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Orders.DataProvider.Persistence;

namespace Orders.DataProvider.Extensions
{
    public static class DatabaseExtension
    {
        public static void AddDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            var configSection = configuration.GetSection("DatabaseSettings");
            var databaseConfig = new DatabaseConfig();
            configSection.Bind(databaseConfig);

            var sb = new StringBuilder(databaseConfig.ConnectionString);
            sb.Append("Database=").Append(databaseConfig.Database);

            var connectionString = sb.ToString();

            services.AddDbContext<DatabaseContext>(opt => { opt.UseNpgsql(connectionString); },
                                                   ServiceLifetime.Transient);
        }
    }

    public class AppDbContextFactory : IDesignTimeDbContextFactory<DatabaseContext>
    {
        public DatabaseContext CreateDbContext(string[] args)
        {
            var OptionsBuilder = new DbContextOptionsBuilder<DatabaseContext>();
            OptionsBuilder.UseNpgsql("Host=localhost;Port=5434;Database=OrdersDb;Username=admin;Password=admin1234");
            return new DatabaseContext(OptionsBuilder.Options);
        }
    }
}
