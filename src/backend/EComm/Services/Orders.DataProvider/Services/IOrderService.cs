using Application.Common.Models;
using Application.Features.Order;

using Domain.Events;

namespace Orders.DataProvider.Services
{
    public interface IOrderService
    {
        Task<Result<OrderCreatedEvent>> Create(CreateOrder dto);
        Task<Result<OrderCreatedEvent>> Delete(DeleteOrder dto);
        Task<Result<OrderCreatedEvent>> Get(GetOrder dto);
        Task<Result<List<OrderCreatedEvent>>> GetAll(GetOrders dto);
    }
}