using Application.Common.Models;
using Application.Features.Order;

using AutoMapper;

using Domain.Entities;
using Domain.Events;

using Orders.DataProvider.Repositories;

namespace Orders.DataProvider.Services
{
    public class OrderService : IOrderService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IOrderRepository _serviceRepository;
        public OrderService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _serviceRepository = _unitOfWork.OrderRepository;
        }

        public async Task<Result<OrderCreatedEvent>> Create(CreateOrder dto)
        {
            var existedEntity = await _serviceRepository.FindByCondition(e => e.Name == dto.Name && e.UserId == dto.UserId);

            if (existedEntity.Count == 0 || existedEntity == null)
            {
                var newEntity = _mapper.Map<Order>(dto);

                try
                {
                    await _serviceRepository.Create(newEntity);
                    await _unitOfWork.SaveAsync();

                    var newCreatedEvent = _mapper.Map<OrderCreatedEvent>(newEntity);
                    return Result<OrderCreatedEvent>.Success(newCreatedEvent);
                }
                catch (Exception ex)
                {
                    return Result<OrderCreatedEvent>.Failure($"Object {typeof(Order)} creation error - {ex.Message}");
                }

            }
            return Result<OrderCreatedEvent>.Failure($"Object {typeof(Order)} creation error - such object already exists");
        }

        public async Task<Result<OrderCreatedEvent>> Delete(DeleteOrder dto)
        {
            var existedEntity = await _serviceRepository.FindByCondition(e => e.Id == dto.Id);

            if (existedEntity.Count == 0 || existedEntity == null)
            {
                return Result<OrderCreatedEvent>.Failure($"Error deleting object {typeof(Order)} - no such object exists");
            }

            try
            {
                var deletedEntity = existedEntity.FirstOrDefault();
                await _serviceRepository.Delete(deletedEntity!);
                await _unitOfWork.SaveAsync();

                var newCreatedEvent = _mapper.Map<OrderCreatedEvent>(deletedEntity);
                return Result<OrderCreatedEvent>.Success(newCreatedEvent);
            }
            catch (Exception ex)
            {
                return Result<OrderCreatedEvent>.Failure($"Error deleting object {typeof(Order)} - {ex.Message}");
            }
        }

        public async Task<Result<OrderCreatedEvent>> Get(GetOrder dto)
        {
            var existedEntity = await _serviceRepository.FindByCondition(e => e.Id == dto.Id);

            if (existedEntity.Count == 0 || existedEntity == null)
            {
                return Result<OrderCreatedEvent>.Failure($"Object {typeof(Order)} request failed - no such object exists");
            }

            var requestedEntity = existedEntity.FirstOrDefault();

            var newCreatedEvent = _mapper.Map<OrderCreatedEvent>(requestedEntity);
            return Result<OrderCreatedEvent>.Success(newCreatedEvent);
        }

        public async Task<Result<List<OrderCreatedEvent>>> GetAll(GetOrders dto)
        {
            var existedEntities = await _serviceRepository.FindAll();

            if (existedEntities.Count == 0 || existedEntities == null)
            {
                return Result<List<OrderCreatedEvent>>.Failure($"Object {typeof(Order)} request failed - no such objects exists");
            }

            var newCreatedEvent = _mapper.Map<List<OrderCreatedEvent>>(existedEntities);
            return Result<List<OrderCreatedEvent>>.Success(newCreatedEvent);
        }
    }
}