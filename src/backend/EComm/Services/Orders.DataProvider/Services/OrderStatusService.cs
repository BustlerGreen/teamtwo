using Application.Common.Models;
using Application.Features.Order;
using AutoMapper;
using Domain.Entities;
using Domain.Events;
using Orders.DataProvider.Repositories;

namespace Orders.DataProvider.Services
{
    public class OrderStatusService : IOrderStatusService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IOrderStatusRepository _serviceRepository;
        public OrderStatusService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _serviceRepository = _unitOfWork.OrderStatusRepository;
        }
        public async Task<Result<OrderStatusCreatedEvent>> Create(CreateOrderStatus dto)
        {
            var existedEntity = await _serviceRepository.FindByCondition(e => e.Value == dto.Value && e.Key == dto.Key);

            if (existedEntity.Count == 0 || existedEntity == null)
            {
                var newEntity = _mapper.Map<OrderStatus>(dto);

                try
                {
                    await _serviceRepository.Create(newEntity);
                    await _unitOfWork.SaveAsync();

                    var newCreatedEvent = _mapper.Map<OrderStatusCreatedEvent>(newEntity);
                    return Result<OrderStatusCreatedEvent>.Success(newCreatedEvent);
                }
                catch (Exception ex)
                {
                    return Result<OrderStatusCreatedEvent>.Failure($"Object {typeof(OrderStatus)} creation error - {ex.Message}");
                }

            }
            return Result<OrderStatusCreatedEvent>.Failure($"Object {typeof(OrderStatus)} creation error - such object already exists");
        }

        public async Task<Result<OrderStatusCreatedEvent>> Delete(DeleteOrderStatus dto)
        {
            var existedEntity = await _serviceRepository.FindByCondition(e => e.Id == dto.Id);

            if (existedEntity.Count == 0 || existedEntity == null)
            {
                return Result<OrderStatusCreatedEvent>.Failure($"Error deleting object {typeof(OrderStatus)} - no such object exists");
            }

            try
            {
                var deletedEntity = existedEntity.FirstOrDefault();
                await _serviceRepository.Delete(deletedEntity!);
                await _unitOfWork.SaveAsync();

                var newCreatedEvent = _mapper.Map<OrderStatusCreatedEvent>(deletedEntity);
                return Result<OrderStatusCreatedEvent>.Success(newCreatedEvent);
            }
            catch (Exception ex)
            {
                return Result<OrderStatusCreatedEvent>.Failure($"Error deleting object {typeof(OrderStatus)} - {ex.Message}");
            }
        }

        public async Task<Result<OrderStatusCreatedEvent>> Get(GetOrderStatus dto)
        {
            var existedEntity = await _serviceRepository.FindByCondition(e => e.Id == dto.Id);

            if (existedEntity.Count == 0 || existedEntity == null)
            {
                return Result<OrderStatusCreatedEvent>.Failure($"Object {typeof(OrderStatus)} request failed - no such object exists");
            }

            var requestedEntity = existedEntity.FirstOrDefault();

            var newCreatedEvent = _mapper.Map<OrderStatusCreatedEvent>(requestedEntity);
            return Result<OrderStatusCreatedEvent>.Success(newCreatedEvent);
        }

        public async Task<Result<List<OrderStatusCreatedEvent>>> GetAll(GetOrderStatuses dto)
        {
            var existedEntities = await _serviceRepository.FindAll();

            if (existedEntities.Count == 0 || existedEntities == null)
            {
                return Result<List<OrderStatusCreatedEvent>>.Failure($"Object {typeof(OrderStatus)} request failed - no such objects exists");
            }

            var newCreatedEvent = _mapper.Map<List<OrderStatusCreatedEvent>>(existedEntities);
            return Result<List<OrderStatusCreatedEvent>>.Success(newCreatedEvent);
        }
    }
}