using Application.Common.Models;
using Application.Features.Order;
using Domain.Events;

namespace Orders.DataProvider.Services
{
    public interface IOrderItemService
    {
        Task<Result<OrderItemCreatedEvent>> Create(CreateOrderItem dto);
        Task<Result<OrderItemCreatedEvent>> Delete(DeleteOrderItem dto);
        Task<Result<OrderItemCreatedEvent>> Get(GetOrderItem dto);
        Task<Result<List<OrderItemCreatedEvent>>> GetAll(GetOrderItems dto);
    }
}