using Application.Common.Models;
using Application.Features.Order;
using Domain.Events;

namespace Orders.DataProvider.Services
{
    public interface IOrderStatusService
    {
        Task<Result<OrderStatusCreatedEvent>> Create(CreateOrderStatus dto);
        Task<Result<OrderStatusCreatedEvent>> Delete(DeleteOrderStatus dto);
        Task<Result<OrderStatusCreatedEvent>> Get(GetOrderStatus dto);
        Task<Result<List<OrderStatusCreatedEvent>>> GetAll(GetOrderStatuses dto);
    }
}