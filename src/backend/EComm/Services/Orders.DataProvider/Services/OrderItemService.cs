using Application.Common.Models;
using Application.Features.Order;
using AutoMapper;
using Domain.Entities;
using Domain.Events;
using Orders.DataProvider.Repositories;

namespace Orders.DataProvider.Services
{
    public class OrderItemService : IOrderItemService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IOrderItemRepository _serviceRepository;
        public OrderItemService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _serviceRepository = _unitOfWork.OrderItemRepository;
        }

        public async Task<Result<OrderItemCreatedEvent>> Create(CreateOrderItem dto)
        {
            var existedEntity = await _serviceRepository.FindByCondition(e => e.ProductId == dto.ProductId && e.Name == dto.Name);

            if (existedEntity.Count == 0 || existedEntity == null)
            {
                var newEntity = _mapper.Map<OrderItem>(dto);

                try
                {
                    await _serviceRepository.Create(newEntity);
                    await _unitOfWork.SaveAsync();

                    var newCreatedEvent = _mapper.Map<OrderItemCreatedEvent>(newEntity);
                    return Result<OrderItemCreatedEvent>.Success(newCreatedEvent);
                }
                catch (Exception ex)
                {
                    return Result<OrderItemCreatedEvent>.Failure($"Object {typeof(OrderItem)} creation error - {ex.Message}");
                }

            }
            return Result<OrderItemCreatedEvent>.Failure($"Object {typeof(OrderItem)} creation error - such object already exists");
        }

        public async Task<Result<OrderItemCreatedEvent>> Delete(DeleteOrderItem dto)
        {
            var existedEntity = await _serviceRepository.FindByCondition(e => e.Id == dto.Id);

            if (existedEntity.Count == 0 || existedEntity == null)
            {
                return Result<OrderItemCreatedEvent>.Failure($"Error deleting object {typeof(OrderItem)} - no such object exists");
            }

            try
            {
                var deletedEntity = existedEntity.FirstOrDefault();
                await _serviceRepository.Delete(deletedEntity!);
                await _unitOfWork.SaveAsync();

                var newCreatedEvent = _mapper.Map<OrderItemCreatedEvent>(deletedEntity);
                return Result<OrderItemCreatedEvent>.Success(newCreatedEvent);
            }
            catch (Exception ex)
            {
                return Result<OrderItemCreatedEvent>.Failure($"Error deleting object {typeof(OrderItem)} - {ex.Message}");
            }
        }

        public async Task<Result<OrderItemCreatedEvent>> Get(GetOrderItem dto)
        {
            var existedEntity = await _serviceRepository.FindByCondition(e => e.Id == dto.Id);

            if (existedEntity.Count == 0 || existedEntity == null)
            {
                return Result<OrderItemCreatedEvent>.Failure($"Object {typeof(OrderItem)} request failed - no such object exists");
            }

            var requestedEntity = existedEntity.FirstOrDefault();

            var newCreatedEvent = _mapper.Map<OrderItemCreatedEvent>(requestedEntity);
            return Result<OrderItemCreatedEvent>.Success(newCreatedEvent);
        }

        public async Task<Result<List<OrderItemCreatedEvent>>> GetAll(GetOrderItems dto)
        {
            var existedEntities = await _serviceRepository.FindAll();

            if (existedEntities.Count == 0 || existedEntities == null)
            {
                return Result<List<OrderItemCreatedEvent>>.Failure($"Object {typeof(OrderItem)} request failed - no such objects exists");
            }

            var newCreatedEvent = _mapper.Map<List<OrderItemCreatedEvent>>(existedEntities);
            return Result<List<OrderItemCreatedEvent>>.Success(newCreatedEvent);
        }
    }
}