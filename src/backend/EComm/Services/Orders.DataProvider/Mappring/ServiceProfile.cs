using Application.Features.Order;
using AutoMapper;
using Domain.Entities;
using Domain.Events;

namespace Orders.DataProvider.Mappring
{
    public class ServiceProfile : Profile
    {
        public ServiceProfile()
        {
            CreateMap<CreateOrder, Order>().ReverseMap();
            CreateMap<OrderCreatedEvent, Order>().ReverseMap();
            CreateMap<List<OrderCreatedEvent>, List<Order>>().ReverseMap();

            CreateMap<CreateOrderItem, OrderItem>().ReverseMap();
            CreateMap<OrderItemCreatedEvent, OrderItem>().ReverseMap();
            CreateMap<List<OrderItemCreatedEvent>, List<OrderItem>>().ReverseMap();

            CreateMap<CreateOrderStatus, OrderStatus>().ReverseMap();
            CreateMap<OrderStatusCreatedEvent, OrderStatus>().ReverseMap();
            CreateMap<List<OrderStatusCreatedEvent>, List<OrderStatus>>().ReverseMap();
        }
    }
}