using System.Linq.Expressions;
using Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using Orders.DataProvider.Persistence;

namespace Orders.DataProvider.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected readonly DatabaseContext _dbContext;
        protected readonly DbSet<T> _dbSet;
        public GenericRepository(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<T>();
        }

        public async Task<List<T>> FindAll()
        {
            return await _dbSet.ToListAsync();
        }
        public async Task<List<T>> FindByCondition(Expression<Func<T, bool>> expression) => await _dbSet.Where(expression)
            .ToListAsync();
        public async Task<T> Create(T entity)
        {
            var result = await _dbSet.AddAsync(entity);
            return entity;
        }
        public Task Update(T entity)
        {
            _dbSet.Update(entity);
            return Task.CompletedTask;
        }
        public Task Delete(T entity)
        {
            _dbSet.Remove(entity);
            return Task.CompletedTask;
        }

        public async Task AddRangeAsync(List<T> entities)
        {
            await _dbSet.AddRangeAsync(entities);
        }
    }
}