using Orders.DataProvider.Persistence;

#nullable disable

namespace Orders.DataProvider.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DatabaseContext _context;
        public UnitOfWork(DatabaseContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        #region Properties
        private OrderRepository _orderRepository;
        public IOrderRepository OrderRepository => _orderRepository ?? (_orderRepository = new OrderRepository(_context));

        private OrderItemRepository _orderItemRepository;
        public IOrderItemRepository OrderItemRepository => _orderItemRepository ?? (_orderItemRepository = new OrderItemRepository(_context));

        private OrderStatusRepository _orderStatusRepository;
        public IOrderStatusRepository OrderStatusRepository => _orderStatusRepository ?? (_orderStatusRepository = new OrderStatusRepository(_context));
        #endregion

        #region Methods
        public async Task SaveAsync() => await _context.SaveChangesAsync();
        #endregion

        #region Implements IDisposable
        public void Dispose()
        {
            _context.Dispose();
        }
        #endregion
    }
}