using Domain.Entities;
using Orders.DataProvider.Persistence;

namespace Orders.DataProvider.Repositories
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        public OrderRepository(DatabaseContext context) : base(context)
        {
        }
    }
}