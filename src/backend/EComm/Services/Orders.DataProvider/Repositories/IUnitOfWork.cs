namespace Orders.DataProvider.Repositories
{
    public interface IUnitOfWork
    {
        IOrderRepository OrderRepository { get; }
        IOrderItemRepository OrderItemRepository { get; }
        IOrderStatusRepository OrderStatusRepository { get; }
        Task SaveAsync();
    }
}