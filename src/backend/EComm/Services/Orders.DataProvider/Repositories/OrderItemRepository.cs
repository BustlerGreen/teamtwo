using Domain.Entities;
using Orders.DataProvider.Persistence;

namespace Orders.DataProvider.Repositories
{
    public class OrderItemRepository : GenericRepository<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(DatabaseContext context) : base(context)
        {
        }
    }
}