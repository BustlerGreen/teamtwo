using Domain.Entities;
using Infrastructure.Persistence;

namespace Orders.DataProvider.Repositories
{
    public interface IOrderStatusRepository : IGenericRepository<OrderStatus>
    {

    }
}