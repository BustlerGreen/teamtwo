using Domain.Entities;
using Orders.DataProvider.Persistence;

namespace Orders.DataProvider.Repositories
{
    public class OrderStatusRepository : GenericRepository<OrderStatus>, IOrderStatusRepository
    {
        public OrderStatusRepository(DatabaseContext context) : base(context)
        {
        }
    }
}