using Infrastructure.EventBus.Options;
using Infrastructure.Persistence;

using MassTransit;

using Orders.Api.Handlers;
using Orders.DataProvider.Extensions;
using Orders.DataProvider.Mappring;
using Orders.DataProvider.Repositories;
using Orders.DataProvider.Services;

namespace Orders.Api.Extensions
{
    public static class ApplicationServiceExtensions
    {
        public static IServiceCollection AddApplicationService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDatabase(configuration);

            services.AddAutoMapper(typeof(ServiceProfile).Assembly);

            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));

            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IOrderItemRepository, OrderItemRepository>();
            services.AddScoped<IOrderStatusRepository, OrderStatusRepository>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IOrderItemService, OrderItemService>();
            services.AddScoped<IOrderStatusService, OrderStatusService>();

            services.AddScoped<CreateOrderHandler>();
            services.AddScoped<DeleteOrderHandler>();
            services.AddScoped<GetOrderHandler>();
            services.AddScoped<GetOrdersHandler>();

            services.AddScoped<CreateOrderItemHandler>();
            services.AddScoped<DeleteOrderItemHandler>();
            services.AddScoped<GetOrderItemHandler>();
            services.AddScoped<GetOrderItemsHandler>();

            services.AddScoped<CreateOrderStatusHandler>();
            services.AddScoped<DeleteOrderStatusHandler>();
            services.AddScoped<GetOrderStatusHandler>();
            services.AddScoped<GetOrderStatusesHandler>();

            var rabbitmqOption = new RabbitMqOption();
            configuration.GetSection("RabbitMq").Bind(rabbitmqOption);

            services.AddMassTransit(x =>
            {
                x.AddConsumer<CreateOrderHandler>();
                x.AddConsumer<DeleteOrderHandler>();
                x.AddConsumer<GetOrderHandler>();
                x.AddConsumer<GetOrdersHandler>();

                x.AddConsumer<CreateOrderItemHandler>();
                x.AddConsumer<DeleteOrderItemHandler>();
                x.AddConsumer<GetOrderItemHandler>();
                x.AddConsumer<GetOrderItemsHandler>();

                x.AddConsumer<CreateOrderStatusHandler>();
                x.AddConsumer<DeleteOrderStatusHandler>();
                x.AddConsumer<GetOrderStatusHandler>();
                x.AddConsumer<GetOrderStatusesHandler>();

                x.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    cfg.Host(new Uri(rabbitmqOption.ConnectionString), hostconfig =>
                    {
                        hostconfig.Username(rabbitmqOption.Username);
                        hostconfig.Password(rabbitmqOption.Password);
                    });

                    cfg.ReceiveEndpoint("create_order", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<CreateOrderHandler>(provider);
                    });
                    cfg.ReceiveEndpoint("delete_order", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<DeleteOrderHandler>(provider);
                    });
                    cfg.ReceiveEndpoint("get_order", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<GetOrderHandler>(provider);
                    });
                    cfg.ReceiveEndpoint("get_orders", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<GetOrderHandler>(provider);
                    });

                    cfg.ReceiveEndpoint("create_order_item", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<CreateOrderItemHandler>(provider);
                    });
                    cfg.ReceiveEndpoint("delete_order_item", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<DeleteOrderItemHandler>(provider);
                    });
                    cfg.ReceiveEndpoint("get_order_item", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<GetOrderItemHandler>(provider);
                    });
                    cfg.ReceiveEndpoint("get_order_items", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<GetOrderItemsHandler>(provider);
                    });

                    cfg.ReceiveEndpoint("create_order_status", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<CreateOrderStatusHandler>(provider);
                    });
                    cfg.ReceiveEndpoint("delete_order_status", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<DeleteOrderStatusHandler>(provider);
                    });
                    cfg.ReceiveEndpoint("get_order_status", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<GetOrderStatusHandler>(provider);
                    });
                    cfg.ReceiveEndpoint("get_order_statuses", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<GetOrderStatusesHandler>(provider);
                    });
                }));
            });
            return services;
        }
    }
}