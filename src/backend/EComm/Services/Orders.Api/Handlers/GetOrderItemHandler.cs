using Application.Common.Models;
using Application.Features.Order;

using Domain.Events;

using MassTransit;

using Orders.DataProvider.Services;

namespace Orders.Api.Handlers
{
    public class GetOrderItemHandler : IConsumer<GetOrderItem>
    {
        private readonly IOrderItemService _service;

        public GetOrderItemHandler(IOrderItemService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<GetOrderItem> context)
        {
            var result = await _service.Get(context!.Message!);

            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(GetOrderHandler)} execution issues ");
            }

            await context.RespondAsync<Result<OrderCreatedEvent>>(result!);

        }
    }
}