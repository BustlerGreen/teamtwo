using Application.Common.Models;
using Application.Features.Order;

using Domain.Events;

using MassTransit;

using Orders.DataProvider.Services;

namespace Orders.Api.Handlers
{
    public class GetOrderStatusHandler : IConsumer<GetOrderStatus>
    {
        private readonly IOrderStatusService _service;

        public GetOrderStatusHandler(IOrderStatusService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<GetOrderStatus> context)
        {
            var result = await _service.Get(context!.Message!);

            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(GetOrderHandler)} execution issues ");
            }

            await context.RespondAsync<Result<OrderCreatedEvent>>(result!);

        }
    }
}