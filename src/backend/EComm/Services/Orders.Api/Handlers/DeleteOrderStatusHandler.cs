using Application.Common.Models;
using Application.Features.Order;

using Domain.Events;

using MassTransit;

using Orders.DataProvider.Services;

namespace Orders.Api.Handlers
{
    public class DeleteOrderStatusHandler : IConsumer<DeleteOrderStatus>
    {
        private readonly IOrderStatusService _service;

        public DeleteOrderStatusHandler(IOrderStatusService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<DeleteOrderStatus> context)
        {
            var result = await _service.Delete(context!.Message!);

            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(DeleteOrderHandler)} execution issues ");
            }

            await context.RespondAsync<Result<OrderCreatedEvent>>(result!);

        }
    }
}