using Application.Common.Models;
using Application.Features.Order;

using Domain.Events;

using MassTransit;

using Orders.DataProvider.Services;

namespace Orders.Api.Handlers
{
    public class GetOrderItemsHandler : IConsumer<GetOrderItems>
    {
        private readonly IOrderItemService _service;

        public GetOrderItemsHandler(IOrderItemService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<GetOrderItems> context)
        {
            var result = await _service.GetAll(context!.Message!);

            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(GetOrdersHandler)} execution issues ");
            }

            await context.RespondAsync<Result<List<OrderCreatedEvent>>>(result);
        }
    }
}