using Application.Common.Models;
using Application.Features.Order;

using Domain.Events;

using MassTransit;

using Orders.DataProvider.Services;

namespace Orders.Api.Handlers
{
    public class DeleteOrderItemHandler : IConsumer<DeleteOrderItem>
    {
        private readonly IOrderItemService _service;

        public DeleteOrderItemHandler(IOrderItemService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<DeleteOrderItem> context)
        {
            var result = await _service.Delete(context!.Message!);

            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(DeleteOrderItemHandler)} execution issues ");
            }

            await context.RespondAsync<Result<OrderCreatedEvent>>(result!);

        }
    }
}