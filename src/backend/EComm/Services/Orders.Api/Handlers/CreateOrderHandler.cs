using Application.Common.Models;
using Application.Features.Order;

using Domain.Events;

using MassTransit;

using Orders.DataProvider.Services;

namespace Orders.Api.Handlers
{
    public class CreateOrderHandler : IConsumer<CreateOrder>
    {
        private readonly IOrderService _service;

        public CreateOrderHandler(IOrderService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<CreateOrder> context)
        {
            var result = await _service.Create(context!.Message!);

            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(CreateOrderItemHandler)} execution issues ");
            }

            await context.RespondAsync<Result<OrderCreatedEvent>>(result!);

        }
    }
}