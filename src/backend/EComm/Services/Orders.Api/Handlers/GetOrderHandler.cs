using Application.Common.Models;
using Application.Features.Order;

using Domain.Events;

using MassTransit;

using Orders.DataProvider.Services;

namespace Orders.Api.Handlers
{
    public class GetOrderHandler : IConsumer<GetOrder>
    {
        private readonly IOrderService _service;

        public GetOrderHandler(IOrderService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<GetOrder> context)
        {
            var result = await _service.Get(context!.Message!);

            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(GetOrderHandler)} execution issues ");
            }

            await context.RespondAsync<Result<OrderCreatedEvent>>(result!);

        }
    }
}