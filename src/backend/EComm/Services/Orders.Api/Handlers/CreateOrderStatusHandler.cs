using Application.Common.Models;
using Application.Features.Order;

using Domain.Events;

using MassTransit;

using Orders.DataProvider.Services;

namespace Orders.Api.Handlers
{
    public class CreateOrderStatusHandler : IConsumer<CreateOrderStatus>
    {
        private readonly IOrderStatusService _service;

        public CreateOrderStatusHandler(IOrderStatusService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<CreateOrderStatus> context)
        {
            var result = await _service.Create(context!.Message!);

            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(CreateOrderItemHandler)} execution issues ");
            }

            await context.RespondAsync<Result<OrderCreatedEvent>>(result!);

        }
    }
}