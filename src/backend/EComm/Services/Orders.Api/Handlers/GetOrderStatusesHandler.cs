using Application.Common.Models;
using Application.Features.Order;

using Domain.Events;

using MassTransit;

using Orders.DataProvider.Services;

namespace Orders.Api.Handlers
{
    public class GetOrderStatusesHandler : IConsumer<GetOrderStatuses>
    {
        private readonly IOrderStatusService _service;

        public GetOrderStatusesHandler(IOrderStatusService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<GetOrderStatuses> context)
        {
            var result = await _service.GetAll(context!.Message!);

            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(GetOrdersHandler)} execution issues ");
            }

            await context.RespondAsync<Result<List<OrderCreatedEvent>>>(result);
        }
    }
}