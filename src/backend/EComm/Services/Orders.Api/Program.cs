using Infrastructure.Logging;
using Microsoft.EntityFrameworkCore;
using Orders.Api.Extensions;
using Orders.DataProvider.Persistence;
using Orders.DataProvider.Repositories;

using Serilog;

var builder = WebApplication.CreateBuilder(args);

// Миграция dotnet ef migrations add InitialCreate -p .\Services\Orders.DataProvider\ -s .\Services\Orders.Api\
//! при создании миграции вручную интструментом DotNet EF необходимо закомментировать
//builder.Host.UseSerilog(SeriLogger.Configure);

// Для поддержки DateTime
AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

builder.Services.AddApplicationService(builder.Configuration);


builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

#region Миграции & первичное заполнение БД
using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    try
    {
        var context = services.GetRequiredService<DatabaseContext>();
        context.Database.Migrate();

        var unitOfWork = services.GetRequiredService<IUnitOfWork>();
        await DatabaseInitializer.InitializeAsync(unitOfWork);
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
}
#endregion

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
