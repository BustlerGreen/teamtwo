using Domain.Entities;

using Identity.Api.Handlers;
using Identity.DataProvider.Extensions;
using Identity.DataProvider.Persistence;
using Identity.DataProvider.Repositories;
using Identity.DataProvider.Services;

using Infrastructure.Authentication;
using Infrastructure.EventBus.Options;

using MassTransit;

using Microsoft.AspNetCore.Identity;

namespace Identity.Api.Extensions
{
    public static class ApplicationServiceExtensions
    {
        public static IServiceCollection AddApplicationService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDatabase(configuration);
            services.AddIdentity<User, IdentityRole<Guid>>(options =>
            {
                options.SignIn.RequireConfirmedAccount = true;

                options.Lockout.AllowedForNewUsers = true;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(10);
                options.Lockout.MaxFailedAccessAttempts = 3;

                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireDigit = true;

                options.User.RequireUniqueEmail = true;

                // default - abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+
                options.User.AllowedUserNameCharacters = ".@abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            })
                .AddEntityFrameworkStores<DatabaseContext>()
                .AddDefaultTokenProviders();
            services.AddJwt(configuration);

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserService, UserService>();

            services.AddScoped<CreateUserHandler>();
            services.AddScoped<LoginUserHandler>();
            services.AddScoped<GetUserByEmailHandler>();
            services.AddScoped<GetUserByUsernameHandler>();
            services.AddScoped<RefreshTokenHandler>();

            var rabbitmqOption = new RabbitMqOption();
            configuration.GetSection("RabbitMq").Bind(rabbitmqOption);

            services.AddMassTransit(x =>
            {
                x.AddConsumer<CreateUserHandler>();
                x.AddConsumer<LoginUserHandler>();
                x.AddConsumer<GetUserByEmailHandler>();
                x.AddConsumer<GetUserByUsernameHandler>();
                x.AddConsumer<RefreshTokenHandler>();

                x.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    cfg.Host(new Uri(rabbitmqOption.ConnectionString), hostconfig =>
                    {
                        hostconfig.Username(rabbitmqOption.Username);
                        hostconfig.Password(rabbitmqOption.Password);
                    });

                    cfg.ReceiveEndpoint("user_create", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<CreateUserHandler>(provider);
                    });

                    cfg.ReceiveEndpoint("user_login", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<LoginUserHandler>(provider);
                    });
                    cfg.ReceiveEndpoint("user_get_by_email", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<GetUserByEmailHandler>(provider);
                    });
                    cfg.ReceiveEndpoint("user_get_by_username", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<GetUserByUsernameHandler>(provider);
                    });
                    cfg.ReceiveEndpoint("user_refresh_token", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<RefreshTokenHandler>(provider);
                    });
                }));
            });
            return services;
        }
    }
}