using Domain.Entities;

using Identity.Api.Extensions;
using Identity.DataProvider.Persistence;

using Infrastructure.Logging;

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

using Serilog;

var builder = WebApplication.CreateBuilder(args);

// Для поддержки DateTime
AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

/// Миграция БД
/// Условия:
///  1. Здесь используется EF Core Tools
///  2. Установка производится командой "dotnet tool install --global dotnet-ef"
///  3. Обновление производится командой "dotnet tool update --global dotnet-ef"
///  4. Пути в команде прописаны из расчета что мы находимся в корневой папке решения
///  ! Перед миграцией необходимо закомментировать строку "builder.Host.UseSerilog(SeriLogger.Configure);"
/// Команда:
/// dotnet ef migrations add InitialCreate -p .\Services\Identity.DataProvider\ -s .\Services\Identity.Api\
builder.Host.UseSerilog(SeriLogger.Configure);


// Add services to the container.
builder.Services.AddApplicationService(builder.Configuration);

//builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
//builder.Services.AddSwaggerGen();

var app = builder.Build();

#region Миграции & первичное заполнение БД
using var scope = app.Services.CreateScope();
var services = scope.ServiceProvider;
try
{
    var context = services.GetRequiredService<DatabaseContext>();
    context.Database.Migrate();
    var userManager = services.GetRequiredService<UserManager<User>>();
    var roleManager = services.GetRequiredService<RoleManager<IdentityRole<Guid>>>();
    await DatabaseInitializer.InitializeAsync(userManager, roleManager);
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}
#endregion

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}

app.UseAuthorization();

//app.MapControllers();

app.Run();
