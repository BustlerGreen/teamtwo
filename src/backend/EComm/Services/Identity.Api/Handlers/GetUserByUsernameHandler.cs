using Application.Common.Models;
using Application.Features.Identity;

using Domain.Events;

using Identity.DataProvider.Services;

using MassTransit;

namespace Identity.Api.Handlers
{
    public class GetUserByUsernameHandler : IConsumer<GetUserByUsername>
    {
        private readonly IUserService _userService;
        public GetUserByUsernameHandler(IUserService userService)
        {
            _userService = userService;
        }

        public async Task Consume(ConsumeContext<GetUserByUsername> context)
        {
            var result = await _userService.GetByUsername(context.Message);
            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(GetUserByUsernameHandler)} execution issues ");
            }
            await context.RespondAsync<Result<UserCreatedEvent>>(result);
        }
    }
}