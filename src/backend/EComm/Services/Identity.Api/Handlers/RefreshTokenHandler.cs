using Application.Common.Models;
using Application.Features.Identity;

using Domain.Events;

using Identity.DataProvider.Services;

using MassTransit;

namespace Identity.Api.Handlers
{
    public class RefreshTokenHandler : IConsumer<RefreshTokenUser>
    {
        private readonly IUserService _userService;
        public RefreshTokenHandler(IUserService userService)
        {
            _userService = userService;
        }
        public async Task Consume(ConsumeContext<RefreshTokenUser> context)
        {
            var result = await _userService.RefreshToken(context.Message);
            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(RefreshTokenHandler)} execution issues ");
            }
            await context.RespondAsync<Result<UserCreatedEvent>>(result);
        }
    }
}