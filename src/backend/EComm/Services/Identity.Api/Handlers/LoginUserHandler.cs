using Application.Common.Models;
using Application.Features.Identity;

using Domain.Events;

using Identity.DataProvider.Services;

using MassTransit;

namespace Identity.Api.Handlers
{
    public class LoginUserHandler : IConsumer<LoginUser>
    {
        private readonly IUserService _userService;

        public LoginUserHandler(IUserService userService)
        {
            _userService = userService;
        }

        public async Task Consume(ConsumeContext<LoginUser> context)
        {
            var result = await _userService.Login(context.Message);
            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(LoginUserHandler)} execution issues ");
            }
            await context.RespondAsync<Result<UserCreatedEvent>>(result);
        }
    }
}