using Application.Common.Models;
using Application.Features.Identity;

using Domain.Events;

using Identity.DataProvider.Services;

using MassTransit;

namespace Identity.Api.Handlers
{
    public class CreateUserHandler : IConsumer<CreateUser>
    {
        private readonly IUserService _userService;
        public CreateUserHandler(IUserService userService)
        {
            _userService = userService;
        }

        public async Task Consume(ConsumeContext<CreateUser> context)
        {
            var result = await _userService.Create(context.Message);
            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(CreateUserHandler)} execution issues ");
            }

            await context.RespondAsync<Result<UserCreatedEvent>>(result);
        }
    }
}