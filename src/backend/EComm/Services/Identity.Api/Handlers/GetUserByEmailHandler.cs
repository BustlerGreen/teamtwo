using Application.Common.Models;
using Application.Features.Identity;

using Domain.Events;

using Identity.DataProvider.Services;

using MassTransit;

namespace Identity.Api.Handlers
{
    public class GetUserByEmailHandler : IConsumer<GetUserByEmail>
    {
        private readonly IUserService _userService;
        public GetUserByEmailHandler(IUserService userService)
        {
            _userService = userService;
        }

        public async Task Consume(ConsumeContext<GetUserByEmail> context)
        {
            var result = await _userService.GetByEmail(context.Message);
            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(GetUserByEmailHandler)} execution issues ");
            }
            await context.RespondAsync<Result<UserCreatedEvent>>(result);
        }
    }
}