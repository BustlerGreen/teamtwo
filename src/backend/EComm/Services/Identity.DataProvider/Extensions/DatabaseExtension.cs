using System.Text;

using Identity.DataProvider.Persistence;

using Infrastructure.Persistence;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Identity.DataProvider.Extensions
{
    public static class DatabaseExtension
    {
        public static void AddDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            var configSection = configuration.GetSection("DatabaseSettings");
            var databaseConfig = new DatabaseConfig();
            configSection.Bind(databaseConfig);

            var sb = new StringBuilder(databaseConfig.ConnectionString);
            sb.Append("Database=").Append(databaseConfig.Database);

            var connectionString = sb.ToString();

            services.AddDbContext<DatabaseContext>(opt => opt.UseNpgsql(connectionString), ServiceLifetime.Transient);
        }
    }
}