using Domain.Entities;
using Domain.Events;

namespace Identity.DataProvider.Extensions
{
    public static class UserExtension
    {
        public static UserCreatedEvent UserToUserCreatedEvent(this User user, string token, IList<string> roles) => new UserCreatedEvent
        {
            Id = user.Id,
            Username = user.UserName!,
            Email = user.Email!,
            Token = token,
            Roles = roles!
        };
        public static UserCreatedEvent UserToUserCreatedEvent(this User user, IList<string> roles) => new UserCreatedEvent
        {
            Id = user.Id,
            Username = user.UserName!,
            Email = user.Email!,
            Roles = roles!
        };
    }
}