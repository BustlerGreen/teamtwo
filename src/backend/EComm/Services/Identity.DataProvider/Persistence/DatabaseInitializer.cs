using Domain.Entities;

using Microsoft.AspNetCore.Identity;

namespace Identity.DataProvider.Persistence
{
    public class DatabaseInitializer
    {
        public static async Task InitializeAsync(UserManager<User> userManager, RoleManager<IdentityRole<Guid>> roleManager)
        {
            Console.WriteLine("Попытка миграции БД...");

            if (!userManager.Users.Any() && !roleManager.Roles.Any())
            {
                var password = "P@$$w0rd!!";
                var phone = "+7-1234567890";

                var newRoles = new List<IdentityRole<Guid>>
                {
                   new IdentityRole<Guid> { Name = "Administrator" },
                   new IdentityRole<Guid> { Name = "Customer" }
                };

                var newUsers = new List<User>
                {
                    new User
                    {
                        UserName = "Admin",
                        Email = "Admin@gov.com",
                        PhoneNumber = phone,
                        EmailConfirmed = true
                    },
                    new User
                    {
                        UserName = "Customer",
                        Email = "Customer@gov.com",
                        PhoneNumber = phone,
                        EmailConfirmed = true
                    }
                };

                foreach (var role in newRoles)
                {
                    await roleManager.CreateAsync(role);
                }

                foreach (var user in newUsers)
                {
                    await userManager.CreateAsync(user, password);
                }

                await userManager.AddToRoleAsync(newUsers[0], newRoles[0].Name!);
                await userManager.AddToRoleAsync(newUsers[1], newRoles[1].Name!);
            }
        }
    }
}