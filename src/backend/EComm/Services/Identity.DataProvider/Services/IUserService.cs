using Application.Common.Models;
using Application.Features.Identity;

using Domain.Events;

namespace Identity.DataProvider.Services
{
    public interface IUserService
    {
        Task<Result<UserCreatedEvent>> Create(CreateUser createUser);
        Task<Result<UserCreatedEvent>> Login(LoginUser loginUser);
        Task<Result<UserCreatedEvent>> GetByUsername(GetUserByUsername username);
        Task<Result<UserCreatedEvent>> GetByEmail(GetUserByEmail email);
        Task<Result<UserCreatedEvent>> RefreshToken(RefreshTokenUser refreshTokenUser);
    }
}