using Application.Common.Models;
using Application.Features.Identity;

using Domain.Entities;
using Domain.Events;

using Identity.DataProvider.Extensions;
using Identity.DataProvider.Repositories;

using Infrastructure.Authentication;

namespace Identity.DataProvider.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IAuthenticationHandler _authenticationHandler;

        public UserService(IUserRepository userRepository, IAuthenticationHandler authenticationHandler)
        {
            _userRepository = userRepository;
            _authenticationHandler = authenticationHandler;
        }

        public async Task<Result<UserCreatedEvent>> Create(CreateUser createUser)
        {
            var existedUser = await _userRepository.GetByUsernameAsync(createUser.Username);

            if (existedUser == null || existedUser.Id == Guid.Empty)
            {
                var user = new User(createUser.Username, createUser.Email, createUser.Phone);

                user.EmailConfirmed = true; // Если не подтвердить адрес - то не проходит авторизация

                var result = await _userRepository.CreateUserAsync(user, createUser.Password);

                if (result.Succeeded)
                {
                    var refreshToken = await SetRefreshTokens(user);

                    var roles = await _userRepository.GetRolesAsync(user);
                    var token = _authenticationHandler.CreateAuthToken(user, roles);

                    var userCreatedEvent = user.UserToUserCreatedEvent(token, roles);

                    userCreatedEvent.RefreshToken = refreshToken;

                    return Result<UserCreatedEvent>.Success(userCreatedEvent);
                };
            }

            return Result<UserCreatedEvent>.Failure("User Registration Issues");
        }

        public async Task<Result<UserCreatedEvent>> Login(LoginUser loginUser)
        {
            var user = await _userRepository.GetByUsernameAsync(loginUser.Username);

            if (user == null) return Result<UserCreatedEvent>.Failure("User not found");

            var result = await _userRepository.CheckPasswordSignInAsync(user, loginUser.Password);

            if (result.Succeeded)
            {
                var refreshToken = await SetRefreshTokens(user);

                var roles = await _userRepository.GetRolesAsync(user);
                var token = _authenticationHandler.CreateAuthToken(user, roles);

                var userCreatedEvent = user.UserToUserCreatedEvent(token, roles);

                userCreatedEvent.RefreshToken = refreshToken;

                return Result<UserCreatedEvent>.Success(userCreatedEvent);
            }

            return Result<UserCreatedEvent>.Failure("The username or password you entered is incorrect");
        }

        public async Task<Result<UserCreatedEvent>> GetByEmail(GetUserByEmail request)
        {
            var user = await _userRepository.GetByEmailAsync(request.Email);

            if (user == null) return Result<UserCreatedEvent>.Failure("User with this email address not found");

            var refreshToken = await SetRefreshTokens(user);

            var roles = await _userRepository.GetRolesAsync(user);
            var token = _authenticationHandler.CreateAuthToken(user, roles);

            var userCreatedEvent = user.UserToUserCreatedEvent(token, roles);

            userCreatedEvent.RefreshToken = refreshToken;

            return Result<UserCreatedEvent>.Success(userCreatedEvent);
        }

        public async Task<Result<UserCreatedEvent>> GetByUsername(GetUserByUsername request)
        {
            var user = await _userRepository.GetByUsernameAsync(request.Username);

            if (user == null) return Result<UserCreatedEvent>.Failure("User with this name not found");

            var refreshToken = await SetRefreshTokens(user);

            var roles = await _userRepository.GetRolesAsync(user);
            var token = _authenticationHandler.CreateAuthToken(user, roles);

            var userCreatedEvent = user.UserToUserCreatedEvent(token, roles);

            userCreatedEvent.RefreshToken = refreshToken;

            return Result<UserCreatedEvent>.Success(userCreatedEvent);
        }

        public async Task<Result<UserCreatedEvent>> RefreshToken(RefreshTokenUser refreshTokenUser)
        {
            var user = await _userRepository.GetByUsernameWithRefreshTokensAsync(refreshTokenUser.Username);
            if (user == null) return Result<UserCreatedEvent>.Failure("User with this name not found");

            var oldToken = user.RefreshTokens.SingleOrDefault(x => x.Token == refreshTokenUser.Token);
            if (oldToken != null && !oldToken.IsActive) return Result<UserCreatedEvent>.Failure("User with this name is not authorized");

            var roles = await _userRepository.GetRolesAsync(user);
            var token = _authenticationHandler.CreateAuthToken(user, roles);

            var userCreatedEvent = user.UserToUserCreatedEvent(token, roles);

            return Result<UserCreatedEvent>.Success(userCreatedEvent);
        }

        private async Task<string> SetRefreshTokens(User user)
        {
            var refreshToken = _authenticationHandler.GenerateRefreshToken();
            await _userRepository.SetRefreshTokensAsync(user, refreshToken);
            return refreshToken.Token;
        }
    }
}