using Domain.Entities;

using Microsoft.AspNetCore.Identity;

namespace Identity.DataProvider.Repositories
{
    public interface IUserRepository
    {
        Task<IdentityResult> CreateUserAsync(User user, string password);
        Task<IList<string>> GetRolesAsync(User user);
        Task<User?> GetByUsernameAsync(string username);
        Task<User?> GetByUsernameWithRefreshTokensAsync(string username);
        Task<User?> GetByEmailAsync(string email);
        Task<SignInResult> CheckPasswordSignInAsync(User user, string password);
        Task SetRefreshTokensAsync(User user, RefreshToken refreshToken);
    }
}