using Domain.Entities;

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Identity.DataProvider.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public UserRepository(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;

        }
        public async Task<IdentityResult> CreateUserAsync(User user, string password) => await _userManager.CreateAsync(user, password);
        public async Task<IList<string>> GetRolesAsync(User user) => await _userManager.GetRolesAsync(user);
        public async Task<User?> GetByUsernameAsync(string username) => await _userManager.Users.FirstOrDefaultAsync(x => x.NormalizedUserName == username.Trim().ToUpper());
        public async Task<User?> GetByUsernameWithRefreshTokensAsync(string username) => await _userManager.Users.Include(r => r.RefreshTokens).FirstOrDefaultAsync(x => x.NormalizedUserName == username.Trim().ToUpper());
        public async Task<User?> GetByEmailAsync(string email) => await _userManager.Users.FirstOrDefaultAsync(x => x.NormalizedEmail == email.Trim().ToUpper());
        public async Task<SignInResult> CheckPasswordSignInAsync(User user, string password) => await _signInManager.CheckPasswordSignInAsync(user, password, false);
        public async Task SetRefreshTokensAsync(User user, RefreshToken refreshToken)
        {
            user.RefreshTokens.Add(refreshToken);
            await _userManager.UpdateAsync(user);
        }
    }
}