﻿using Domain.Entities;

using Microsoft.AspNetCore.Identity;

namespace Basket.DataAccess.Repositories
{
    public interface IBasketRepository
    {
        public Task<ShoppingCart> GetAsync(Guid Id);
        public Task<IdentityResult> CreateAsync(ShoppingCart cart);
        
        public Task<ShoppingCart> UpdateAsync(ShoppingCart cart);
        public Task<IdentityResult> DeleteAsync(Guid Id);
        

    }
}