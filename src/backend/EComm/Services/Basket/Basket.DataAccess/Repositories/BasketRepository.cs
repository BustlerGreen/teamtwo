﻿using System.Text.Json;
using Domain.Entities;

using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Caching.Distributed;

namespace Basket.DataAccess.Repositories
{
    public class BasketRepository : IBasketRepository
    {
        private readonly IDistributedCache _redisCache;
        
        public BasketRepository(IDistributedCache redisCache)
        {
            _redisCache = redisCache ?? throw new ArgumentNullException(nameof(redisCache));
        }
       
        public async Task<ShoppingCart> GetAsync(Guid Id)
        {
            var basket = await _redisCache.GetStringAsync(Id.ToString());
            if(String.IsNullOrEmpty(basket)) { return null; }
            var res  = JsonSerializer.Deserialize<ShoppingCart>(basket, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
            return res;
        }
      
        public async Task<IdentityResult> CreateAsync(ShoppingCart cart)
        {
            if (null == cart) return IdentityResult.Failed();
            await _redisCache.SetStringAsync(cart.Id.ToString(), JsonSerializer.Serialize(cart), 
                new DistributedCacheEntryOptions() { SlidingExpiration = TimeSpan.FromDays(2) });
            return IdentityResult.Success;
        }

        
        public async Task<ShoppingCart> UpdateAsync(ShoppingCart cart)
        {
            if (null == cart) return null;
            await _redisCache.SetStringAsync(cart.Id.ToString(), JsonSerializer.Serialize(cart),
                new DistributedCacheEntryOptions() { SlidingExpiration = TimeSpan.FromDays(2) });
            return await GetAsync(cart.Id);
        }
        
        public async Task<IdentityResult> DeleteAsync(Guid Id)
        {
            await _redisCache.RemoveAsync(Id.ToString());
            return IdentityResult.Success;
        }
    }
}
