﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Domain.Entities;

using Microsoft.AspNetCore.Identity;

namespace Basket.DataAccess.BLLService
{
    public interface IBasketService
    {
        public Task<IdentityResult> CreateCartAsync(ShoppingCart cart);
        public Task<ShoppingCart> GetCartAsync(Guid Id);
        public Task<IdentityResult> RemoveCartAsync(Guid Id);
        public Task<ShoppingCart> AddCartItemAsync(Guid Id, ShoppingCartItem item);
        public Task<ShoppingCart> RemoveCartItemAsync(Guid Id, ShoppingCartItem item);


        
    }
}
