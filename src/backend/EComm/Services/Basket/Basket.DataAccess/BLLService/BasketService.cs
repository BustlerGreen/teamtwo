﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

using Domain.Entities;
using Basket.DataAccess.Repositories;

using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Caching.Distributed;

using Microsoft.Extensions.Caching.StackExchangeRedis;

namespace Basket.DataAccess.BLLService
{
    public class BasketService : IBasketService
    {
        private readonly IBasketRepository _basketRepository;

        public BasketService(IBasketRepository basketRepository)
        {
            _basketRepository = basketRepository;
        }

        public async Task<IdentityResult> CreateCartAsync(ShoppingCart cart)
        {
            if(null == cart.Items) cart.Items = new List<ShoppingCartItem>();
            var res = await _basketRepository.CreateAsync(cart);
            return res;
        }
        
        public async Task<ShoppingCart> GetCartAsync(Guid Id)
        { 
            return await _basketRepository.GetAsync(Id);
        }
        
        public async Task<IdentityResult> RemoveCartAsync(Guid Id)
        {
            return await _basketRepository.DeleteAsync(Id);
        }

        public async Task<ShoppingCart> AddCartItemAsync(Guid Id, ShoppingCartItem item)
        {
            var cart = await _basketRepository.GetAsync(Id);
            if (null == cart) return null;
            var CartItem = cart.Items.FirstOrDefault(item => item.Id == item.Id);
            if (null == CartItem) cart.Items.Add(item);
            else CartItem.Quantity += item.Quantity;
            return await _basketRepository.UpdateAsync(cart);
        }


        public async Task<ShoppingCart> RemoveCartItemAsync(Guid Id, ShoppingCartItem item)
        {
            var cart = await _basketRepository.GetAsync(Id);
            if (null == cart) return null;
            var CartItem = cart.Items.FirstOrDefault(item => item.Id == item.Id);
            if (null != CartItem)
            {
                CartItem.Quantity -= item.Quantity;
                if(0 >=  CartItem.Quantity) cart.Items.Remove(CartItem);
            }
            return await _basketRepository.UpdateAsync(cart);
        }
    }
}
