﻿using Basket.DataAccess.BLLService;

using Infrastructure.Contracts;

using MassTransit;

namespace Basket.Api.MTConsumers
{
    public class RemoveCartConsumer : IConsumer<RemoveCartMessage>
    {
        private readonly IBasketService _basketService;

        public RemoveCartConsumer(IBasketService basketService)
        {
            _basketService = basketService;
        }

        public async Task Consume(ConsumeContext<RemoveCartMessage> context)
        {
            await _basketService.RemoveCartAsync(context.Message.Id);
        }
    }
}
