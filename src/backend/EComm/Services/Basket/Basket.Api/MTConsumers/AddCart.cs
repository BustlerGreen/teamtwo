﻿using Basket.DataAccess.BLLService;
using Infrastructure.Contracts;
using Domain.Entities;
using MassTransit;

namespace Basket.Api.MTConsumers
{
    public class AddCartConsumer : IConsumer<AddCartMessage>
    {
        private readonly IBasketService _basketService;

        public AddCartConsumer(IBasketService basketService)
        {
            _basketService = basketService;
        }

        public async Task Consume(ConsumeContext<AddCartMessage> context)
        {
            if ((null == context) | (null == context.Message)) { };
            var cart = new ShoppingCart()
            {
                Id = context.Message.Id,
                UserName = context.Message.UserName,
                Items = context.Message.Items.Select(item => new ShoppingCartItem()
                {
                    Id = item.Id,
                    Name = item.Name,
                    Price = item.Price,
                    Quantity = item.Qunatity
                }).ToList(),
            };

            var res = await _basketService.CreateCartAsync(cart);

            //await context.RespondAsync(new CartStatusMessage()
            //{
            //    Id = cart.Id,
            //    CartItems = cart.Items.Select(item => new CartItemMessage()
            //    {
            //        Id = item.Id,
            //        Name = item.Name,
            //        Price = item.Price,
            //        Qunatity= item.Quantity
            //    }).ToList()
            //}) ;
        }
    }
}
