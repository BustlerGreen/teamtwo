﻿using Basket.DataAccess.BLLService;

using Infrastructure.Contracts;

using MassTransit;

namespace Basket.Api.MTConsumers
{
    public class GetCartConsumer : IConsumer<GetCartMessage>
    {
        private readonly IBasketService _basketService;

        public GetCartConsumer(IBasketService basketService)
        {
            _basketService = basketService;
        }

        public async Task Consume(ConsumeContext<GetCartMessage> context)
        {
            var cartStMsg = new CartStatusMessage();
            var cart = await _basketService.GetCartAsync(context.Message.Id);
            if(null != cart)
            {
                cartStMsg.Id = cart.Id;
                cartStMsg.CartItems = cart.Items.Select(item => new CartItemMessage()
                {
                    Id = item.Id,
                    Price = item.Price,
                    Qunatity = item.Quantity,
                    Name = item.Name,
                }).ToList();
            }
            await context.RespondAsync(cartStMsg);
        }
    }
}
