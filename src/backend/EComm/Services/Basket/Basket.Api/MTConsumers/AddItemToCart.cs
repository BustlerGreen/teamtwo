﻿
using Domain.Entities;
using Infrastructure.Contracts;
using Basket.DataAccess.BLLService;

using MassTransit;

namespace Basket.Api.MTConsumers
{
    public class AddItemToCartConsumer : IConsumer<AddItemToCartMessage>
    {
        private readonly IBasketService _basketService;

        public AddItemToCartConsumer(IBasketService basketService)
        {
            _basketService = basketService;
        }
        public async Task Consume(ConsumeContext<AddItemToCartMessage> context)
        {
            var cart = await _basketService.AddCartItemAsync(context.Message.CartId, 
                new ShoppingCartItem()
                {
                    Id = context.Message.ItemId,
                    Name= context.Message.Name,
                    Price= context.Message.Price,
                    Quantity = 1,
                });

            //await context.RespondAsync(new CartStatusMessage()
            //{
            //    Id = cart.Id,
            //    CartItems = cart.Items.Select(item => new CartItemMessage()
            //    {
            //        Id = item.Id,
            //        Name = item.Name,
            //        Price = item.Price,
            //        Qunatity = item.Quantity,
            //    }).ToList()
            //}); ;

        }
    }
}
