using Basket.DataAccess.Repositories;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using MassTransit;
using Infrastructure.EventBus.Options;
using Basket.Api.MTConsumers;
using Polly;
using Basket.DataAccess.BLLService;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDistributedRedisCache(options =>
{
    options.Configuration = builder.Configuration.GetConnectionString("redisCashe");
});
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title="basket api", Version="v1" });
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Description = "JWT authorization <br/> Enter Bearer token <br/> format: Bearer accesstoken",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer"
    }); ; ;
    c.AddSecurityRequirement(new OpenApiSecurityRequirement()
    {
        {
            new OpenApiSecurityScheme
            {
                Reference= new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id= "Bearer"
                },
                Scheme = "outh2",
                Name = "Bearer",
                In = ParameterLocation.Header,
            },
            new List<string>()
        }
    });
});

builder.Services.AddAuthentication("Bearer")
    .AddJwtBearer("Bearer", options =>
    {
        options.Authority = @"https://localhost:7063";
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateAudience = false
        };
    });
builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("ReadCart", policy =>
    {
        policy.RequireAuthenticatedUser();
        policy.RequireClaim("scope", "ecomm.Read");
    });
    options.AddPolicy("WriteCart", policy =>
    {
        policy.RequireAuthenticatedUser();
        policy.RequireClaim("scope", "ecomm.Write");
    });
});
//builder.Services.AddScoped<AddCartConsumer>();
//builder.Services.AddScoped<RemoveCartConsumer>();
builder.Services.AddScoped(typeof(IBasketService), typeof(BasketService));
builder.Services.AddScoped(typeof(IBasketRepository), typeof(BasketRepository));

builder.Services.AddMassTransit(busConfigurator =>
{
    //busConfigurator.AddConsumer<AddCartConsumer>();
    //busConfigurator.AddConsumer<AddItemToCartConsumer>();
    busConfigurator.AddConsumer<AddCartConsumer>();
    busConfigurator.AddConsumer<GetCartConsumer>();
    busConfigurator.AddConsumer<RemoveCartConsumer>();
    busConfigurator.AddBus(regContext => Bus.Factory.CreateUsingRabbitMq(factoryCfg =>
    {
        var rabbitOption = new RabbitMqOption();
        builder.Configuration.GetSection("rabbitmq").Bind(rabbitOption);

        factoryCfg.Host(rabbitOption.ConnectionString, rabbitcfg =>
        {
            rabbitcfg.Username(rabbitOption.Username);
            rabbitcfg.Password(rabbitOption.Password);
        });
        factoryCfg.ReceiveEndpoint("addbasket", ep =>
        {
            ep.PrefetchCount = 16;
            ep.UseMessageRetry(r => r.Interval(3, 1000));
            ep.ConfigureConsumer<AddCartConsumer>(regContext);
        });
        factoryCfg.ReceiveEndpoint("getbasket", ep =>
        {
            ep.PrefetchCount = 16;
            ep.UseMessageRetry(r => r.Interval(3, 1000));
            ep.ConfigureConsumer<GetCartConsumer>(regContext);
        });
        factoryCfg.ReceiveEndpoint("removebasket", ep =>
        {
            ep.PrefetchCount = 16;
            ep.UseMessageRetry(r => r.Interval(3, 1000));
            ep.ConfigureConsumer<RemoveCartConsumer>(regContext);
        });
    }));
});
var app = builder.Build();
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();
app.Run();
