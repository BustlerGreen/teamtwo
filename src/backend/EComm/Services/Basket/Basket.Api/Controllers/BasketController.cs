﻿using Domain.Entities;
using Basket.DataAccess.BLLService;
using Basket.DataAccess.Repositories;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Basket.Api.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/v1/[controller]")]
    public class BasketController : ControllerBase
    {
        private readonly IBasketRepository _repo;
        private readonly IBasketService _serv;

        public BasketController(IBasketRepository repo, IBasketService serv)
        { 
            _repo = repo;
            _serv = serv;
        }

        [HttpGet("{Id:guid}")]
        [Authorize(Policy ="ReadCart")]
        public async Task<ActionResult<ShoppingCart>> GetCartById(Guid Id)
        {
            //var res = await _repo.GetAsync(Id);
            var res = await _serv.GetCartAsync(Id);
            if (null == res) return NotFound();
            return Ok(res);
        }

        [HttpPost]
        public async Task<ActionResult<ShoppingCart>> CreateCart(ShoppingCart cart)
        {
            if(null == cart) return BadRequest();
            //var res = await _repo.CreateAsync(cart);
            var res = await _serv.CreateCartAsync(cart);
            if (null == res) return NotFound();
            return Ok(res);
        }

        [HttpPut]
        public async Task<ActionResult<ShoppingCart>> UpdateCart(ShoppingCart cart)
        {
            if (null == cart) return BadRequest();
            //var res = await _repo.UpdateAsync(cart);
            var res = await _serv.CreateCartAsync(cart);
            if (null == res) return NotFound();
            return Ok(res);
        }

        [HttpDelete("{Id:guid}")]
        public async Task<ActionResult> DeleteCart(Guid Id)
        {
            //var res = await _repo.GetAsync(Id);
            var res = await _serv.GetCartAsync(Id);
            if (null == res) return NotFound();
            //await _repo.DeleteAsync(res.Id);
            await _serv.RemoveCartAsync(res.Id);
            return Ok();
        }
    }
}
