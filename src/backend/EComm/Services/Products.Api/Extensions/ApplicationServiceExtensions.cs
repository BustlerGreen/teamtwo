using Infrastructure.EventBus.Options;
using Infrastructure.Persistence;

using MassTransit;

using Products.Api.Handlers;
using Products.DataProvider.Extensions;
using Products.DataProvider.Mappring;
using Products.DataProvider.Repositories;
using Products.DataProvider.Services;

namespace Products.Api.Extensions
{
    public static class ApplicationServiceExtensions
    {
        public static IServiceCollection AddApplicationService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDatabase(configuration);

            services.AddAutoMapper(typeof(ServiceProfile).Assembly);

            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));

            services.AddScoped<IProductRepository, ProductRepository>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<IProductService, ProductService>();

            //services.AddScoped<CreateProductHandler>();
            //services.AddScoped<DeleteProductHandler>();
            //services.AddScoped<GetProductHandler>();
            //services.AddScoped<GetProductsHandler>();

            var rabbitmqOption = new RabbitMqOption();
            configuration.GetSection("RabbitMq").Bind(rabbitmqOption);

            services.AddMassTransit(x =>
            {
                x.AddConsumer<CreateProductHandler>();
                x.AddConsumer<DeleteProductHandler>();
                x.AddConsumer<GetProductHandler>();
                x.AddConsumer<GetProductsHandler>();
                x.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    cfg.Host(new Uri(rabbitmqOption.ConnectionString), hostconfig =>
                    {
                        hostconfig.Username(rabbitmqOption.Username);
                        hostconfig.Password(rabbitmqOption.Password);
                    });

                    cfg.ReceiveEndpoint("create_product", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<CreateProductHandler>(provider);
                    });
                    cfg.ReceiveEndpoint("delete_product", ep =>
                   {
                       ep.PrefetchCount = 16;
                       ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                       ep.ConfigureConsumer<DeleteProductHandler>(provider);
                   });
                    cfg.ReceiveEndpoint("get_product", ep =>
                    {
                        ep.PrefetchCount = 16;
                        ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                        ep.ConfigureConsumer<GetProductHandler>(provider);
                    });
                    cfg.ReceiveEndpoint("get_products", ep =>
                   {
                       ep.PrefetchCount = 16;
                       ep.UseMessageRetry(retryConfig => { retryConfig.Interval(2, 100); });
                       ep.ConfigureConsumer<GetProductsHandler>(provider);
                   });
                }));
            });
            return services;
        }
    }
}