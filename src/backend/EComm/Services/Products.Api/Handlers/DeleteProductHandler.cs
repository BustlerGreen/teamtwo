using Application.Common.Models;
using Application.Features.Product;

using Domain.Events;

using MassTransit;

using Products.DataProvider.Services;

namespace Products.Api.Handlers
{
    public class DeleteProductHandler : IConsumer<DeleteProduct>
    {
        private readonly IProductService _service;

        public DeleteProductHandler(IProductService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<DeleteProduct> context)
        {
            var result = await _service.Delete(context!.Message!);

            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(DeleteProductHandler)} execution issues ");
            }

            await context.RespondAsync<Result<ProductCreatedEvent>>(result!);

        }
    }
}