using Application.Common.Models;
using Application.Features.Product;

using Domain.Events;

using MassTransit;

using Products.DataProvider.Services;

namespace Products.Api.Handlers
{
    public class GetProductHandler : IConsumer<GetProduct>
    {
        private readonly IProductService _service;

        public GetProductHandler(IProductService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<GetProduct> context)
        {
            var result = await _service.Get(context!.Message!);

            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(GetProductHandler)} execution issues ");
            }

            await context.RespondAsync<Result<ProductCreatedEvent>>(result!);

        }
    }
}