using Application.Common.Models;
using Application.Features.Product;
using Domain.Events;
using MassTransit;
using Products.DataProvider.Services;

namespace Products.Api.Handlers
{
    public class GetProductsHandler : IConsumer<GetProducts>
    {
        private readonly IProductService _service;

        public GetProductsHandler(IProductService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<GetProducts> context)
        {
            var result = await _service.GetAll(context!.Message!);

            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(GetProductsHandler)} execution issues ");
            }

            await context.RespondAsync<Result<List<ProductCreatedEvent>>>(result);
        }
    }
}