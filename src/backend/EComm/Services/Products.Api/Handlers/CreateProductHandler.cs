using Application.Common.Models;
using Application.Features.Product;

using Domain.Events;

using MassTransit;

using Products.DataProvider.Services;

namespace Products.Api.Handlers
{
    public class CreateProductHandler : IConsumer<CreateProduct>
    {
        private readonly IProductService _service;

        public CreateProductHandler(IProductService service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<CreateProduct> context)
        {
            var result = await _service.Create(context!.Message!);

            if (result == null)
            {
                throw new InvalidOperationException($"Handler {typeof(CreateProductHandler)} execution issues ");
            }

            await context.RespondAsync<Result<ProductCreatedEvent>>(result!);

        }
    }
}